
# Reset AMC13 T1 & T2 firmwares
AMC13Tool2.exe -c $AMC13_connection -X ./test/AMC13_Reset.cmd
#ws 0x0 0x10			# reset T2 firmware
#wv 0x0 0x0			# reset T1 firmware
sleep 5

# Initialize AMC13 for Clock and trigger distribution
AMC13Tool2.exe -c $AMC13_connection -X ./test/AMC13_Initialize.cmd
#wv 0x8 0x000000004 		# BX offset
#wv 0x0000001c 0x30000000 	# Trigger rule
#wv 0x3 0x8000000 		# ignore DAQ data (TTS still working)
#en 1-12			# enable OT clock for 1-12 AMC slots

ttc log dump > /dev/null

sleep 5
ttc log dump
