
# send synchronous triggers with 50000 BX interval
ttc trg sync start 50000

# poll trigger monitor every 1s for 10s 
for i in {0..10}; do
	sleep 0.1

	# Checking TTC-FC7 event counter
	ttc cnc cnt

	# Dumping the transition logger in TTC-FC7
	ttc log dump

	# Checking AMC13 event counter
	AMC13Tool2.exe -c $AMC13_connection -X ./test/AMC13_TriggerMonitor.cmd #--> for cycle with TTC FC7
done

