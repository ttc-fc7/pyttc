# Uploading firmware to TTC and OT(s)
# run LoadFW.sh

# Configure TTC
ttc configure

# Enable CMS Standard trigger rules in TTC
ttc trg set rule 1

# Switch TTC trigger source to spare pin loopback --> put jumper to spare pins !
ttc fmc set_as_trigger_source
ttc trg stat

# Disable TTS FSM in TTC-FC7
#ttc tts disable_fsm

# Starting orbit tructure
ttc orb enable

# Check TTS state in TTC logger
sleep 1.5
ttc log dump > /dev/null

