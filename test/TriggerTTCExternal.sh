
# Enable external trigger
ttc dio5 set_as_trigger_source
ttc trg src 5
#ttc dio5 set term 2 1
#ttc dio5 set thr_V 2 1.5

# Checking TTC-FC7 event counter
ttc cnc cnt

# Dumping the transition logger in TTC-FC7
ttc log dump

# Checking AMC13 event counter
AMC13Tool2.exe -c $AMC13_connection -X ./test/AMC13_TriggerMonitor.cmd #--> for cycle with TTC FC7

