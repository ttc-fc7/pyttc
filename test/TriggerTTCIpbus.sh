
# Sending 5 triggers
ttc trg ipbus send 5

# Checking TTC-FC7 event counter
ttc cnc cnt

# Dumping the transition logger in TTC-FC7
ttc log dump

# Checking AMC13 event counter
AMC13Tool2.exe -c $AMC13_connection -X ./test/AMC13_TriggerMonitor.cmd #--> for cycle with TTC FC7

