#!/usr/bin/bash
# export Ph2_ACF
export Working_Dir=$(pwd)
export IP_Addr_TTC_FC7=192.168.4.20
export Image_TTC_FC7=ttc-fc7_2023-11-24.bit # !!! only the filename, it is assumed to be in fw-binary subdir as per the README.md
export IP_Addr_Readout_FC7s=(192.168.4.10 192.168.4.30)
export Image_Readout_FC7s=(d19c_2023-09-14.bit d19c_2023-09-14.bit)  # again, only the filename (pyot/test/ subdir is assumed)
export Ph2_ACF_Dir=/path/to/Ph2_ACF
export pyot_Dir=$(pwd)/pyot

cd $Ph2_ACF_Dir
echo $(pwd)
source setup.sh

# change ip address in default board xml
sed "s/target=.*:50001/target=${IP_Addr_TTC_FC7}:50001/g" settings/PS_Module.xml > settings/TTC_FC7.xml
# load fw onto TTC-FC7
exists=`fpgaconfig -l -c settings/TTC_FC7.xml | grep $Image_TTC_FC7 | awk '{ print $5 }'`
if [ "$exists" == "" ]; then
    echo "Image $Image_TTC_FC7 has not been uploaded, yet, doing it now..."
    fpgaconfig -f $Working_Dir/fw-binary/$Image_TTC_FC7 -i $Image_TTC_FC7 -c settings/TTC_FC7.xml
fi
fpgaconfig -i $Image_TTC_FC7 -c settings/TTC_FC7.xml

# load fw onto Readout_FC7 [eventually this could be multiple boards]
for i in "${!IP_Addr_Readout_FC7s[@]}"; do 
    sed "s/target=.*:50001/target=${IP_Addr_Readout_FC7s[$i]}:50001/g" settings/PS_Module.xml > settings/OT_uDTC_$i.xml
    exists=`fpgaconfig -l -c settings/OT_uDTC_$i.xml | grep ${Image_Readout_FC7s[$i]} | awk '{ print $5 }'`
    if [ "$exists" == "" ]; then
	echo "Image ${Image_Readout_FC7s[$i]} has not been uploaded to ${IP_Addr_Readout_FC7s[$i]}, yet, doing it now..."
	fpgaconfig -f $Working_Dir/pyot/test/${Image_Readout_FC7s[$i]} -i ${Image_Readout_FC7s[$i]} -c settings/OT_uDTC_$i.xml
    fi
    fpgaconfig -i ${Image_Readout_FC7s[$i]} -c settings/OT_uDTC_$i.xml
done	
cd $Working_Dir
echo $(pwd)

ot() {
    ip=`echo $1 | sed -n 's/^\([^0-9]*\)\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\(.*\)$/\2/p'`
    if [ "$ip" == "" ]; then
	echo "Usage: ot <IP address> <command(s)>"
	echo "Running command $* on 0.0.0.0"
	IPOT="0.0.0.0" python $pyot_Dir/ot.py $*
    else
	dir=$(pwd)
	cd $pyot_Dir
	shift
	echo "Running command $* on $ip"
	IPOT="$ip" python $pyot_Dir/ot.py $*
	cd $dir
    fi
}

ots() {
    for i in "${!IP_Addr_Readout_FC7s[@]}"; do
	ip="${IP_Addr_Readout_FC7s[$i]}"
	ot $ip $*
    done
}
