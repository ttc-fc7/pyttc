
# Getting started

## Clone to your local directory and verify prerequisites

```console
git clone https://gitlab.cern.ch/ttc-fc7/pyttc.git
cd pyttc
```

This package contains two python programs to control the TTC-FC7: pyTTC.py with the basic functions to interact with the fw via ipbus, ttc.py with the implementation of a simple "command line interface".

The 'ttc' command needs the following packages to be installed (see some help at the bottom of this page):
- the correct ipbus/uhal version (around 2.8 for CC8)
- the correct version of AMC13 tools (around 1.3.4 for CC8)
- python2 or python3
- a Ph2_ACF package or some other way of preparing the sd card and uploading the TTC-FC7 firmware

## Testing the AMC13 and TTC-FC7
Please, review the contenct of env.sh according to the paths of the above packages before proceeding, then source it
```console
source env.sh
```

### AMC13:
Verify that the file amc13-1.3.4/amc13/etc/amc13/connections.xml contains the correct IP address for the AMC13 in your crate. Then check if you can talk to the AMC13:

```console
AMC13Tool2.exe -c $AMC13_connection
>st
>q
```

### Upload the TTC-FC7 firmware for testing
If you do not yet have a TTC-FC7 fw binary, you can get the latest one from the CERN repo
```console
git clone https://gitlab.cern.ch/ttc-fc7/fw-binary.git
```

** Preparation of the sd card: **
If you have an sd card and a Ph2_ACF installed, you can skip this step. Otherwise see instructions on how to install the imgtool and prepare a card with a GoldenImage in ./micro_sd_card.txt

Upload your TTC-FC7 image:
```console
cd <THE IC_MMC DIRECTORY>
sudo ./imgtool /dev/<YOUR SD CARD DEVICE> add <FW IMAGE OF YOUR CHOSING> ./binary/<FW IMAGE OF YOUR CHOSING>
sudo ./imgtool /dev/<YOUR SD CARD DEVICE> check <FW IMAGE OF YOUR CHOSING>
```

** Upload the TTC-FC7 with Ph2_ACF **
The sed command below modifies the IP address in an existing hardware description file, settings/PS_Module.xml, as an example. You can replace it with any files that works in your crate with a uDTC board. If you do not have such an example file, modifying the IP addresss might not be sufficient. The protocol used in this example file
uri="chtcp-2.0://localhost:10203?target=192.168.0.169:50001"
might not work for you. In that case, try 
uri="ipbusudp-2.0://192.168.0.169:50001"
The first version assumes that you are logged into the machine that is directly connected to the crate controller (the MCH), second assumes a hub.

```console
cd <YOUR PH2_ACF DIRECTORY>
source setup.sh
sed "s/target=.*:50001/target=<THE IP ADDRESS OF THE TTC-FC7 BOARD>:50001/g" settings/PS_Module.xml > settings/TTC_FC7.xml
fpgaconfig -l -c settings/TTC_FC7.xml # see the images on the sd card
fpgaconfig -f $OLDPWD/fw-binary/<FW IMAGE OF YOUR CHOSING>.bit -i <FW IMAGE OF YOUR CHOSING>.bit -c settings/TTC_FC7.xml
fpgaconfig -l -c settings/TTC_FC7.xml # verify that the upload worked
fpgaconfig -i <FW IMAGE OF YOUR CHOSING>.bit  -c settings/TTC_FC7.xml # load the fw for the sd card to the FPGA and make it the default
cd -
```

** Check that you can talk to the TTC-FC7
```console
ttc trg stat
ttc log dump 
```

## Run the system within the context of the Phase 2 Tracker.

Scripts in the test/ directory demonstrate the steps to configure a Phase 2 Tracker system with an AMC13, a TTC-FC7, and several uDTC boards and send some triggers. These scripts do not rely on any functionalities of Ph2_ACF other than the firmware upload command and are used to verify changes in the TTC-FC7 and d19c firmware.

See step by step instructions in test/readme.txt

The full integration of the TTC-FC7 board into Ph2_ACF is under construction. A quick solution could be: the functionality of LoadFW.sh and any control of the uDTC boards can be done within Ph2_ACF already. The other shell scripts could be just run from a system call.

# Customization for the CERN setup 

Find IP address here: https://cms-tracker-daq.web.cern.ch/cms-tracker-daq/components/200_fc7_crate_186/
Update IP address in ttc.py line 25: uTTC = pyTTC( "192.168.4.20" )

# Installation of the prerequisites

For a more complete description, see the Ph2_ACF guide https://gitlab.cern.ch/cms_tk_ph2/

Below is a short recipe:

## ipbus/uhal
See what is installed, something like
```console
 yum list installed | grep uhal
 rpm -ql cactuscore-uhal-uhal | grep lib
 whereis uhal
```

** If it is missing, installation in CC8:**
Required packages:
```console
sudo yum install git
sudo yum install boost-devel readline readline-devel
sudo yum install make rpm-build git-core erlang gcc-c++-8.* boost-devel pugixml-devel python36-devel zlib-devel
```

If you do not have epel-release repo available (on your own risk):
```console
curl -s https://packagecloud.io/install/repositories/rabbitmq/erlang/script.rpm.sh | sudo bash
sudo yum install epel-release
```

Install uhal 2.8 from prebuilt rpms:

```console
sudo yum groupremove uhal
sudo curl https://ipbus.web.cern.ch/doc/user/html/_downloads/ipbus-sw.centos8.repo -o /etc/yum.repos.d/ipbus-sw.repo
sudo yum-config-manager --enable powertools
sudo yum clean all
sudo yum groupinstall uhal
```

OR Install uhal v2.8.12 from source:

```console
git clone --depth=1 -b v2.8.12 --recurse-submodules https://github.com/ipbus/ipbus-software.git
cd ipbus-software
sudo ln -s /usr/bin/python3 /usr/bin/python
sudo ln -s /usr/bin/python3-config /usr/bin/python-config
make Set=uhal
sudo make Set=uhal install
```

## AMC13 tools
It should be instaled in the user dir somewhere. If not, install it in the following way:
```console
git clone --recurse-submodules https://gitlab.cern.ch/cms-cactus/boards/amc13.git
cd amc13
git checkout tags/1.3.4
source env.sh
make
```

## Pyhton
Check whether you have python2 or python3, e.g.:
```console
whereis python # to see what python binaries are in your path and in what order they are called
ls -al `which python` # to see which binary runs when python is called
python --version # to see the exact version number
```

## Ph2_ACF
Again, for a complete guide, look at https://gitlab.cern.ch/cms_tk_ph2

However, if all the above work, this should too:
```console
git clone --recurse-submodules https://gitlab.cern.ch/cms_tk_ph2/Ph2_ACF.git
cd Ph2_ACF
source setup.sh
mkdir build
cd build
cmake ..
make -j4
```

# Request changes by emailing to balazs.tamas@wigner.hu or veszpremi.viktor@wigner.hu


