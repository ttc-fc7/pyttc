
# export Cactus uhal
export LD_LIBRARY_PATH=/opt/cactus/lib:$LD_LIBRARY_PATH
export PATH=/opt/cactus/bin:$PATH:./

# export AMC13
export AMC13_toolDir=/path/to/amc13-1.2.9
export AMC13_connection=$AMC13_toolDir/amc13/etc/amc13/connections.xml
# source environment variables 
source $AMC13_toolDir/env.sh

# chose desired python version
alias python='/usr/bin/python2 or python3'



