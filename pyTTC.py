#----------------------------------------------------------------------------------
# file pyTTC.py
#----------------------------------------------------------------------------------
# author : Viktor Veszpremi (viktor.veszpremi@cern.ch)
# contributions from:
#          Tamas Balazs (tamas.balazs@cern.ch)
#          Lajos Palanki (LP618@ic.ac.uk)
#          Alexander Ruede (aruede@cern.ch)
#----------------------------------------------------------------------------------
# version 1.0
#
# Details :
# Library for TTC-FC7 firmware python interface
#
# Last changes:
# 2021.09.07
#---------------------------------------------------------------------------------

import sys
import uhal
from time import sleep

class pyTTC:

    def __init__(self, ipaddr):
        uhal.setLogLevelTo(uhal.LogLevel.WARNING)
        self.ipaddr = ipaddr
	# udp transactions worked before CentOS 7.7 upgrade only !
	# self.hw = uhal.getDevice("FC7", "ipbusudp-2.0://" + self.ipaddr + ":50001", "file://./cfg/device_address_table_fc7.xml")
        self.hw = uhal.getDevice("FC7", "chtcp-2.0://localhost:10203?target=" + self.ipaddr + ":50001", "file://./cfg/device_address_table_fc7.xml")


################################################################
### IPbus
################################################################

    def ipb_read(self, nodeID, dispatch=1):
        word = self.hw.getNode(nodeID).read()
        if dispatch == 1:
        	self.hw.dispatch()
        return int(word)

    def ipb_write(self, nodeID, data, dispatch=1):
        self.hw.getNode(nodeID).write(data)
        if dispatch == 1:
        	self.hw.dispatch()
        return int(data)

    def ipb_dispatch(self):
        return self.hw.dispatch()


################################################################
### General config
################################################################

    def ext_clk(self, bit):
        self.ipb_write("user.ctrl_regs.trigger.ext_clk_en",bit,1)
        print("BUFGMUX Select switched to:" + bin(bit))
	
    def status_clk_gen(self):
        clk_40_locked_1 = self.ipb_read("user.stat_regs.status_clk_gen.clk_40_locked_1",1)
        clk_40_locked_2 = self.ipb_read("user.stat_regs.status_clk_gen.clk_40_locked_2",1)
        ref_clk_locked  = self.ipb_read("user.stat_regs.status_clk_gen.ref_clk_locked",1)
        print ("------------------------------------------------")
        print ("clk_40_locked_1   ", clk_40_locked_1)
        print ("clk_40_locked_2   ", clk_40_locked_2)
        print ("ref_clk_locked    ", ref_clk_locked)
        print ("------------------------------------------------")
        print ("")

    # selecting the source of the clock (0 - coax, 1 - fmc_l8_clk1 external clock DIO5, 2 - fmc_l8_clk0, 3 - internal oscillator(default))
    def clock_source(self, source):
        self.ipb_write("system.reg_ctrl.clock_source",source,1)
        print("Clock source switched to:" + bin(source))




################################################################
### FMC
################################################################
    
    def l12_pwr_on(self):
        self.ipb_write("system.reg_ctrl_2.fmc_l12_pwr_en",1,1)
        self.ipb_write("system.reg_ctrl_2.fmc_pg_c2m",1,1)
        print('L12 FMC power ON')

    def l8_pwr_on(self):
        self.ipb_write("system.reg_ctrl_2.fmc_l8_pwr_en",1,1)
        self.ipb_write("system.reg_ctrl_2.fmc_pg_c2m",1,1)
        print('L8 FMC power ON')

    def fmc_pwr_on(self):
        self.ipb_write("system.reg_ctrl_2.fmc_l12_pwr_en",1,1)
        self.ipb_write("system.reg_ctrl_2.fmc_l8_pwr_en",1,1)
        self.ipb_write("system.reg_ctrl_2.fmc_pg_c2m",1,1)
        print('L8 & L12 FMC power ON')

    def fmc_pwr_off(self):
        self.ipb_write("system.reg_ctrl_2.fmc_l12_pwr_en",0,1)
        self.ipb_write("system.reg_ctrl_2.fmc_l8_pwr_en",0,1)
        self.ipb_write("system.reg_ctrl_2.fmc_pg_c2m",0,1)
        print('L8 & L12 FMC power OFF')

    def set_reset_all(self):
        self.ipb_write("user.ctrl_regs.reset_reg.global_rst",1,1)
        self.ipb_write("user.ctrl_regs.reset_reg.clk_gen_rst",1,1)

    def release_reset_all(self):
        self.ipb_write("user.ctrl_regs.reset_reg.global_rst",0,1)
        self.ipb_write("user.ctrl_regs.reset_reg.clk_gen_rst",0,1)
        sleep(0.4)

    def reset_setup(self):
        print ("Resetting...")
        self.set_reset_all()
        sleep(0.2)
        self.release_reset_all()
        sleep(0.2)
        print("Reset DONE.")
        #sys.exit(1)


################################################################
### Trigger
################################################################

    def send_ipbus_trigger(self, int, n = 1, delay = 1):
        print('Sending single triggers via ipbus')

        for i in range(n):
            self.ipb_write("user.ctrl_regs.trigger.ipb_trigger",1,1)
            print('Trigger ' + str(i) + ' sent')
            sleep(int)
            self.ipb_write("user.ctrl_regs.trigger.ipb_trigger",0,1)
            if (i == n-1):
                break
            sleep(delay)

    def set_trigger_rule(self, src):
        print ('Changing trigger rule to: ' + str(src))
        self.ipb_write("user.ctrl_regs.trigger.trigger_rule",src,1)

    def read_trigger_rule(self):
        val_rule = self.ipb_read("user.ctrl_regs.trigger.trigger_rule",1)
        rule = str('{:032b}'.format(val_rule))
        print ("Active trigger rules <IT><PS><2S><CMS>: ", rule[-4:])

    def set_external_trigger_select(self, sel):
        self.ipb_write("user.ctrl_regs.trigger.external_trigger_select",sel,1)

    def set_external_trigger_from_dio5(self):
        self.set_external_trigger_select(0)

    def set_external_trigger_from_sfp(self):
        self.set_external_trigger_select(1)

    def set_external_trigger_from_fmc_l12_spare_pin7(self):
        self.set_external_trigger_select(2)

    def set_external_trigger_from_backplane(self):
        self.set_external_trigger_select(3)

    def read_external_trigger_select(self):
        val = self.ipb_read("user.ctrl_regs.trigger.external_trigger_select",1)
        if val == 0:
            print ("External trigger is received from DIO5")
        elif val == 1:
            print ("External trigger is received from SFP+ instead of DIO5")
        elif val == 2:
            print ("External trigger is received from FMC L12 spare pin 7 instead of DIO5")
        elif val == 3:
            print ("External trigger is received from AMC slot in backplane")
        else:
            print ("Wrong selection for external trigger input")
        return val

    def enable_triggers(self):
        self.ipb_write("user.ctrl_regs.trigger.trigger_enable",1,1)
        sleep(0.1)
        self.ipb_write("user.ctrl_regs.trigger.trigger_enable",0,1)
        print('Triggering started')

    def disable_triggers(self):
        self.ipb_write("user.ctrl_regs.trigger.trigger_disable",1,1)
        sleep(0.1)
        self.ipb_write("user.ctrl_regs.trigger.trigger_disable",0,1)
        print('Triggering stopped')

    def set_trigger_source(self, src):
        print ('Changing trigger source to: ' + str(src))
        self.ipb_write("user.ctrl_regs.trigger.trigger_source",src,1)

    def read_trigger_source(self):
        src = self.ipb_read("user.ctrl_regs.trigger.trigger_source",1)
        if src == 1:
            src_name = "ipbus"
        elif src == 2:
            src_name = "SFP+ B"
        elif src == 3:
            src_name = "fmc pin7"
        elif src == 4:
            src_name = "backplane"
        elif src == 5:
            src_name = "dio5 ch2"
        elif src == 6:
            src_name = "async_trigger"
        elif src == 7:
            src_name = "clocked_l1a"
        else:
            src_name = "none"
        print ("Active trigger source: ", src_name, "(",src,")")
        return src

    def read_trigger_enable_state(self):
        state = self.ipb_read("user.stat_regs.trigger.trigger_enable_state",1);
        if state == 0:
            state_name = "disabled"
        else:
            state_name = "enabled"
        print ("Trigger ouput is: ", state_name)
        return state

    def set_trigger_interval(self, interval):
        self.ipb_write("user.ctrl_regs.clocked_trigger.trigger_interval",interval,1)
        print("Trigger interval changed to:" + str(interval))

    def check_FIFO(self):
        while True:
            print(bin(self.ipb_read("user.stat_regs.simple_trigger_fifo",1)))

    def set_trigger_latency(self, val):
        print ("Changing trigger latency to:", val)
        self.ipb_write("user.ctrl_regs.trigger2.latency",val,1)

    def read_trigger_latency(self):
        val = self.ipb_read("user.ctrl_regs.trigger2.latency",1)
        print ("Trigger latency set to:", val)
        return val

    def read_l1a_fifo_cnt(self):
        val = self.ipb_read("user.ctrl_regs.l1a_fifo.cnt",1)
        print("Entry count in L1A FIFO:", val)
        return val

    def set_l1a_prescale_cut(self, val):
        self.ipb_write("user.ctrl_regs.trigger_prescale.cut",val,1)

    def read_l1a_prescale_cut(self):
        val=self.ipb_read("user.ctrl_regs.trigger_prescale.cut",1)
        return val

    def read_l1a_avg_rate(self):
        val=self.ipb_read("user.stat_regs.l1a_avg_rate.cnt",1)
        return (val/1459.4048) # [kHz]: div by duration: 3563×25×16384÷1000000

    def set_l1a_count_request(self, val):
        self.ipb_write("user.ctrl_regs.trigger3.L1A_count_request",val,1)
        
    def read_l1a_count_request(self):
        val=self.ipb_read("user.ctrl_regs.trigger3.L1A_count_request",1)
        return val

    def set_l1a_count_request_en(self, val):
        self.ipb_write("user.ctrl_regs.trigger3.L1A_request_en",val,1)
        
    def read_l1a_count_request_en(self):
        val=self.ipb_read("user.ctrl_regs.trigger3.L1A_request_en",1)
        return val


################################################################
### 8SFP FMC
################################################################

    def set_reset_8SFP(self):
        self.ipb_write("user.ctrl_regs.reset_reg.global_rst",1,1)
        self.ipb_write("user.ctrl_regs.reset_reg.clk_gen_rst",1,1)
        #self.ipb_write("user.ctrl_regs.reset_reg.fmc_pll_rst",0,1) #INVERTED!
        #self.ipb_write("user.ctrl_regs.reset_reg.fmc_pll_rst",1,1)
        self.ipb_write("user.ctrl_regs.reset_reg.i2c_rst",1,1)
        print("Global reset & clock genenerator reset & 8SFP FMC i2c reset sent")

    def release_reset_8SFP(self):
        self.ipb_write("user.ctrl_regs.reset_reg.global_rst",0,1)
        self.ipb_write("user.ctrl_regs.reset_reg.clk_gen_rst",0,1)
        sleep(0.5)
        #self.ipb_write("user.ctrl_regs.reset_reg.fmc_pll_rst",1,1) #INVERTED!
        #self.ipb_write("user.ctrl_regs.reset_reg.fmc_pll_rst",0,1)
        sleep(0.5)
        self.ipb_write("user.ctrl_regs.reset_reg.i2c_rst",0,1)
        print("Global reset & clock genenerator reset & 8SFP FMC i2c reset released")

    def status_crt(self):
        sleep(3)
        clk_rate_1 = self.ipb_read("user.stat_regs.clk_rate_1",1)
        clk_rate_2 = self.ipb_read("user.stat_regs.clk_rate_2",1)
        clk_rate_3 = self.ipb_read("user.stat_regs.clk_rate_3",1)
        clk_rate_4 = self.ipb_read("user.stat_regs.clk_rate_4",1)
        clk_rate_5 = self.ipb_read("user.stat_regs.clk_rate_5",1)
        clk_rate_6 = self.ipb_read("user.stat_regs.clk_rate_6",1)
        clk_rate_7 = self.ipb_read("user.stat_regs.clk_rate_7",1)
        clk_rate_8 = self.ipb_read("user.stat_regs.clk_rate_8",1)
        clk_rate_9 = self.ipb_read("user.stat_regs.clk_rate_9",1)
        print ("Measuring clocks in the firmware ...")
        print ("------------------------------------------------")
        print ("clk_rate_1 --> L1A                       ", clk_rate_1)
        print ("clk_rate_2 --> fabric_clk                ", clk_rate_2)
        print ("clk_rate_3 --> clk_40MHz                 ", clk_rate_3)
        print ("clk_rate_4 --> clk_80MHz                 ", clk_rate_4)
        print ("clk_rate_5 --> clk_160MHz                ", clk_rate_5)
        print ("clk_rate_6 --> osc125_a_mgtrefclk_i      ", clk_rate_6)
        print ("clk_rate_7 --> clk_400MHz                ", clk_rate_7)
        print ("clk_rate_8 --> clk_200MHz                ", clk_rate_6)
        print ("clk_rate_9 --> ref_clk_200MHz            ", clk_rate_7)
        print ("------------------------------------------------")
        print ("")

    def i2c_reset(self):
        self.ipb_write("user.ctrl_regs.reset_reg.i2c_rst",1,1)
        sleep(0.1)
        #self.ipb_write("user.ctrl_regs.reset_reg.i2c_rst",0,1)
        print("8SFP i2c master reset sent")

    def i2c_enable(self):
        self.ipb_write("user.ctrl_regs.i2c_settings",0x0083e8,1)
        print("8SFP i2c master enabled")
    
    def i2c_disable(self):
        self.ipb_write("user.ctrl_regs.i2c_settings",0x000000,1)
        print("8SFP i2c master disabled")
   
    def i2c_command(self):
        self.ipb_write("user.ctrl_regs.i2c_command",0x00F40004 | 0x80000000,1)
        self.ipb_write("user.ctrl_regs.i2c_command",0x00F40004,1)
        sleep(0.001)	
        self.ipb_write("user.ctrl_regs.i2c_command",0x00B80000 | 0x80000000,1)
        self.ipb_write("user.ctrl_regs.i2c_command",0x00B80000,1)
        self.ipb_dispatch()
        sleep(0.001)	
        self.ipb_write("user.ctrl_regs.i2c_command",0x00F40000 | 0x80000000,1)
        self.ipb_write("user.ctrl_regs.i2c_command",0x00F40000,1)

    def i2c_reply(self):
        i2c_reply = self.ipb_read("user.stat_regs.i2c_reply",1)
        print ("------------------------------------------------")
        print ("i2c_reply        ", i2c_reply)
        print ("------------------------------------------------")
        print ("")

    def i2c_status(self):
        i2c_settings = self.ipb_read("user.ctrl_regs.i2c_settings",1)
        i2c_command = self.ipb_read("user.ctrl_regs.i2c_command",1)
        i2c_reply = self.ipb_read("user.stat_regs.i2c_reply",1)
        reg_i2c_settings = self.ipb_read("user.stat_regs.reg_i2c_settings",1)
        print ("------------------------------------------------")
        print ("i2c_settings     ", hex(i2c_settings))
        print ("i2c_command      ", hex(i2c_command))
        print ("i2c_reply        ", i2c_reply)
        print ("reg_i2c_settings ", reg_i2c_settings)
        print ("------------------------------------------------")
        print ("")


################################################################
### DIO5
################################################################

    def enable_DIO5(self):
        self.ipb_write("user.ctrl_regs.ext_dio5_tlu_1.dio5_en",1,1)
        print("DIO5 enabled")

    def disable_DIO5(self):
        self.ipb_write("user.ctrl_regs.ext_dio5_tlu_1.dio5_en",0,1)
        print("DIO5 disabled")

    def enable_load_config_DIO5(self):
        self.ipb_write("user.ctrl_regs.ext_dio5_tlu_2.dio5_load_config",1,1)
        print("enable DIO5 load config")

    def disable_load_config_DIO5(self):
        self.ipb_write("user.ctrl_regs.ext_dio5_tlu_2.dio5_load_config",0,1)
        print("disable DIO5 load config")

    def set_io_DIO5(self, config):
        self.ipb_write("user.ctrl_regs.ext_dio5_tlu_1.dio5_ch_out_en",config,1)
        print("DIO5 output configured to:" + bin(config))

    def read_io_DIO5(self):
        config=self.ipb_read("user.ctrl_regs.ext_dio5_tlu_1.dio5_ch_out_en",1)
        return config

    def set_channel_io_DIO5(self, ch, io):
        if (ch>=1 and ch<=5):
            if (ch==3):
                bp=1<<int(ch-1)
                ch_io=self.read_io_DIO5()
                if (io == "OUT"):
                    ch_io|=bp
                else:
                    ch_io&= ~bp

                self.set_io_DIO5(ch_io)
                print("DIO5 channel", ch, "set to", self.read_channel_io_DIO5(ch))
            else:
                print("set_channel_io_DIO5: you can change the direction only in CH3!")
        else:
            print("There are only 5 channels in the DIO5")
            
    def read_channel_io_DIO5(self, ch):
        if (ch>=1 and ch<=5):
            ch_io=self.read_io_DIO5()
            bp=1<<int(ch-1)
            return "IN" if (ch_io&bp == 0) else "OUT"
        else:
            print("There are only 5 channels in the DIO5")
            return "ERR"

    def set_channel_threshold_DIO5(self, ch, thr):
        if ch>=1 and ch<=5 and (thr >= 0 and thr <= 255):
            if ch == 1 or ch == 2:
                self.ipb_write("user.ctrl_regs.ext_dio5_tlu_1.dio5_ch"+str(ch)+"_thr",thr,1)
            elif ch == 3 or ch == 4 or ch == 5:
                self.ipb_write("user.ctrl_regs.ext_dio5_tlu_2.dio5_ch"+str(ch)+"_thr",thr,1)

    def read_channel_threshold_DIO5(self, ch):
        if ch == 1 or ch == 2:
            thr = self.ipb_read("user.ctrl_regs.ext_dio5_tlu_1.dio5_ch"+str(ch)+"_thr", 1)
        elif ch == 3 or ch == 4 or ch == 5:
            thr = self.ipb_read("user.ctrl_regs.ext_dio5_tlu_2.dio5_ch"+str(ch)+"_thr", 1)
        return thr
            

##     def status_DIO5(self):
##         imp_cnt = self.ipb_read("user.stat_regs.imp_cnt",1)
##         print "------------------------------------------------"
##         print "imp_cnt           ", imp_cnt
##         print "------------------------------------------------"
##         print ""


    def set_channel_term_50_DIO5(self, ch, state):
        if ch>=1 and ch<=5 and (state == 0 or state == 1):
            self.ipb_write("user.ctrl_regs.ext_dio5_tlu_1.dio5_ch"+str(ch)+"_term_50ohm_en",state,1)

    def read_channel_term_50_DIO5(self, ch):
        term = self.ipb_read("user.ctrl_regs.ext_dio5_tlu_1.dio5_ch"+str(ch)+"_term_50ohm_en", 1)
        return term


################################################################
### TTS decoder
################################################################
    
    def read_tts_status_tts_state(self):
        TTS_state = self.ipb_read("user.stat_regs.TTS_status.TTS_state",1)
        print ("Last TTS decoded state: ", TTS_state)
        return TTS_state


    def set_tts_fsm_disable_reg(self, bit):
        self.ipb_write("user.ctrl_regs.tts_idelay.tts_fsm_disable",bit,1)
	
    def set_tts_fsm_enable(self):
        self.set_tts_fsm_disable_reg(0)
	
    def set_tts_fsm_disable(self):
        self.set_tts_fsm_disable_reg(1)
	

    def set_tts_idelay_intap(self, intap):
        self.ipb_write("user.ctrl_regs.tts_idelay.intap",intap,1)

    def set_tts_idelay_load(self, bit):
        self.ipb_write("user.ctrl_regs.tts_idelay.load",bit,1)

    def set_tts_idelay_inv(self, bit):
        self.ipb_write("user.ctrl_regs.tts_idelay.inv",bit,1)


    def read_tts_idelay_outtap(self):
        outtap = self.ipb_read("user.stat_regs.tts_idelay.outtap",1)
        print ("TTS decoder input phase delay: ", outtap)
        return outtap

    def read_tts_idelay_load(self):
        load = self.ipb_read("user.ctrl_regs.tts_idelay.load",1)
        print ("TTS decoder input phase delay load: ", load)
        return load

    def read_tts_idelay_inv(self):
        inv = self.ipb_read("user.ctrl_regs.tts_idelay.inv",1)
        print ("TTS decoder input phase delay inv: ", inv)
        return inv


################################################################
### State transition logger
################################################################
    
    def tr_logger_dout(self):
        tr_logger_dout = self.ipb_read("user.stat_regs.tr_logger_dout",1)
        print ("------------------------------------------------")
        print ("State transition event", tr_logger_dout)
        print ("------------------------------------------------")
        print ("")

    def tr_logger_dcnt(self):
        tr_logger_dcnt = self.ipb_read("user.stat_regs.tr_logger.dcnt",1)
        print ("------------------------------------------------")
        print ("Number of state transition log entries ", tr_logger_dcnt)
        print ("------------------------------------------------")
        print ("")

    def tr_logger_block_read(self):
        tr_logger_dcnt = self.ipb_read("user.stat_regs.tr_logger.dcnt",1)
        print ("Number of state transition log entries ", tr_logger_dcnt)
        if tr_logger_dcnt > 0:
            node = self.hw.getNode("user.stat_regs.tr_logger_dout")
            tr_logger_dout = node.readBlock(tr_logger_dcnt)
            self.hw.dispatch()
            # -- print " ", tr_logger_dout
            return tr_logger_dout
        else:
            return []

    def tr_logger_dump(self):
        tr_logger_dout = self.tr_logger_block_read()
        if len(tr_logger_dout) > 0:
            for i in range(len(tr_logger_dout)):
                tr_logger_sel = (tr_logger_dout[i]>>19) & 0b111;
                if (tr_logger_sel == 0b000):
                    print (format(i, '06d'),": ","[BC0], ORB=",(tr_logger_dout[i]>>1)&0x3FFFF,", BC0_en=",tr_logger_dout[i]&0x1)
                elif (tr_logger_sel == 0b001):
                    print (format(i, '06d'),": ","[L1A], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", L1A phase=", format(tr_logger_dout[i]&0x7F, '05d'))

                #elif (tr_logger_sel == 0b010):
                #    print format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd=",tr_logger_dout[i]&0x07F
		          ### 22 bits --> | 3 bit sel | 12 bit bcnt+ChB_proc_sendahead_time-x"011" | 7 bit BGo_last_bcmd |
                elif (tr_logger_sel == 0b010):
                #    print(tr_logger_dout[i]&0x07F)
                    if tr_logger_dout[i]&0x7F == 0x1:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= EC0")
                    elif tr_logger_dout[i]&0x7F == 0x2:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= BC0")
                    elif tr_logger_dout[i]&0x7F == 0x3:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= OC0")
                    elif tr_logger_dout[i]&0x7F == 0x4:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= Resync")
                    elif tr_logger_dout[i]&0x7F == 0x5:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= Reset")
                    elif tr_logger_dout[i]&0x7F == 0x6:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= Hard Reset")
                    elif tr_logger_dout[i]&0x7F == 0x7:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= start L1A")
                    elif tr_logger_dout[i]&0x7F == 0x8:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= stop L1A")
                    elif tr_logger_dout[i]&0x7F == 0x9:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= Cal. trigger")
                    elif tr_logger_dout[i]&0x7F == 0xa:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= EC0+BC0")
                    elif tr_logger_dout[i]&0x7F == 0xb:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= periodic EC0")
                    elif tr_logger_dout[i]&0x7F == 0xc:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= periodic BC0")
                    elif tr_logger_dout[i]&0x7F == 0xd:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= periodic EC0+BC0")
                    elif tr_logger_dout[i]&0x7F == 0xe:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= wait n. BX")
                    elif tr_logger_dout[i]&0x7F == 0xf:
                        print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", ChB Cmd= wait n. ORB")

                #elif (tr_logger_sel == 0b011):
                #    print format(i, '06d'),": ","[TTS], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", TTS=",tr_logger_dout[i]&0x7F
                ### 22 bits --> | 3 bit sel | 12 bit bcnt | 3 bit empty | 4 bit TTS |		
                elif (tr_logger_sel == 0b011):
                #    print(tr_logger_dout[i]&0x7F) --> TTS state
                    if tr_logger_dout[i]&0x7F == 0x2:
                        print (format(i, '06d'),": ","[TTS], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", TTS= OOS(2)")
                    elif tr_logger_dout[i]&0x7F == 0x4:
                        print (format(i, '06d'),": ","[TTS], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", TTS= BSY(4)")
                    elif tr_logger_dout[i]&0x7F == 0x8:
                        print (format(i, '06d'),": ","[TTS], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", TTS= RDY(8)")
                    else:
                        print (format(i, '06d'),": ","[TTS], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", TTS= UNDEF(",tr_logger_dout[i]&0x7F,")")

                #elif (tr_logger_sel == 0b100):
                #    print format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO=",tr_logger_dout[i]&0x7F
		          ### 22 bits --> | 3 bit sel | 12 bit bcnt | 7 bit BGo |
                elif (tr_logger_sel == 0b100):
                #    print(tr_logger_dout[i]&0x7F)
                    if tr_logger_dout[i]&0x7F == 0x1:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= EC0")
                    elif tr_logger_dout[i]&0x7F == 0x2:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= BC0")
                    elif tr_logger_dout[i]&0x7F == 0x3:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= OC0")
                    elif tr_logger_dout[i]&0x7F == 0x4:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= Resync")
                    elif tr_logger_dout[i]&0x7F == 0x5:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= Reset")
                    elif tr_logger_dout[i]&0x7F == 0x10:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= TCDS 0/Start")
                    elif tr_logger_dout[i]&0x7F == 0x11:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= TCDS 1/Stop")
                    elif tr_logger_dout[i]&0x7F == 0x14:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= TCDS 4/Resync")
                    elif tr_logger_dout[i]&0x7F == 0x15:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= TCDS 5/Reset")
                    elif tr_logger_dout[i]&0x7F == 0x17:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= TCDS 7/Sleep")
                    elif tr_logger_dout[i]&0x7F == 0x41:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= immediate ECO")
                    elif tr_logger_dout[i]&0x7F == 0x42:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= immediate BCO")
                    elif tr_logger_dout[i]&0x7F == 0x43:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= immediate OCO")
                    elif tr_logger_dout[i]&0x7F == 0x44:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= immediate Resync")
                    elif tr_logger_dout[i]&0x7F == 0x45:
                        print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", BGO= immediate Reset")

                elif (tr_logger_sel == 0b101):
                    print (format(i, '06d'),": ","[L1R], BCN=",(tr_logger_dout[i]>>7)&0xFFF,", L1R phase=", format(tr_logger_dout[i]&0x7F, '05d'))
                else:
                    print (format(i, '06d'),": ERR! raw:",format(tr_logger_dout[i], '022b'))

    def tr_logger_dump_old(self):
        tr_logger_dcnt = self.ipb_read("user.stat_regs.tr_logger.dcnt",1)
        print ("Number of state transition log entries ", tr_logger_dcnt)
        if tr_logger_dcnt > 0:
            for i in range (0,tr_logger_dcnt):
                  tr_logger_dout = self.ipb_read("user.stat_regs.tr_logger_dout",1)
                  tr_logger_sel = (tr_logger_dout>>19) & 0b111;
                  if (tr_logger_sel == 0b000):
                       print (format(i, '06d'),": ","[BC0], ORB=",(tr_logger_dout>>1)&0x3FFFF,", BC0_en=",tr_logger_dout&0x1," raw:",format(tr_logger_dout, '022b'))
                  elif (tr_logger_sel == 0b001):
                       print (format(i, '06d'),": ","[L1A], BCN=",(tr_logger_dout>>7)&0xFFF,", L1A phase=", format(tr_logger_dout&0x7F, '05b')," raw:",format(tr_logger_dout, '022b'))
                  elif (tr_logger_sel == 0b010):
                       print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout>>7)&0xFFF,", ChB Cmd=",tr_logger_dout&0x07F," raw:",format(tr_logger_dout, '022b'))
                  elif (tr_logger_sel == 0b011):
                       print (format(i, '06d'),": ","[TTS], BCN=",(tr_logger_dout>>7)&0xFFF,", TTS=",tr_logger_dout&0x7F," raw:",format(tr_logger_dout, '022b'))
                  elif (tr_logger_sel == 0b100):
                       print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout>>7)&0xFFF,", BGO=",tr_logger_dout&0x7F," raw:",format(tr_logger_dout, '022b'))
                  elif (tr_logger_sel == 0b101):
                       print (format(i, '06d'),": ","[L1R], BCN=",(tr_logger_dout>>7)&0xFFF,", L1R phase=", format(tr_logger_dout&0x7F, '05b')," raw:",format(tr_logger_dout, '022b'))
                  else:
                       print (format(i, '06d'),": ERR! raw:",format(tr_logger_dout, '022b'))

    def tr_logger_dump_chart(self,pds,cpd,fpd):
        tr_logger_dcnt = self.ipb_read("user.stat_regs.tr_logger.dcnt",1)
        print ("Number of state transition log entries ", tr_logger_dcnt)
        if tr_logger_dcnt > 0:
             for i in range (0,tr_logger_dcnt):
                  tr_logger_dout = self.ipb_read("user.stat_regs.tr_logger_dout",1)
                  tr_logger_sel = (tr_logger_dout>>19) & 0b111;
                  if (tr_logger_sel == 0b000):
                       print (format(i, '06d'),": ","[BC0], ORB=",(tr_logger_dout>>1)&0x3FFFF,", BC0_en=",tr_logger_dout&0x1," raw:",format(tr_logger_dout, '022b'))
                  elif (tr_logger_sel == 0b001):
                       print (format(pds, '06d'), format(cpd, '06d'), format(fpd, '06d'), format(i, '06d'),": ","[L1A], BCN=",(tr_logger_dout>>7)&0xFFF,", L1A phase=", format(tr_logger_dout&0x7F, '05d')," raw:",format(tr_logger_dout, '022b'))
                  elif (tr_logger_sel == 0b010):
                       print (format(i, '06d'),": ","[CHB], BCN=",(tr_logger_dout>>7)&0xFFF,", ChB Cmd=",tr_logger_dout&0x07F," raw:",format(tr_logger_dout, '022b'))
                  elif (tr_logger_sel == 0b011):
                       print (format(i, '06d'),": ","[TTS], BCN=",(tr_logger_dout>>7)&0xFFF,", TTS=",tr_logger_dout&0x7F," raw:",format(tr_logger_dout, '022b'))
                  elif (tr_logger_sel == 0b100):
                       print (format(i, '06d'),": ","[BGO], BCN=",(tr_logger_dout>>7)&0xFFF,", BGO=",tr_logger_dout&0x7F," raw:",format(tr_logger_dout, '022b'))
                  elif (tr_logger_sel == 0b101):
                       print (format(pds, '06d'), format(cpd, '06d'), format(fpd, '06d'), format(i, '06d'),": ","[L1R], BCN=",(tr_logger_dout>>7)&0xFFF,", L1R phase=", format(tr_logger_dout&0x7F, '05d')," raw:",format(tr_logger_dout, '022b'))
                  else:
                       print (format(i, '06d'),": ERR! raw:",format(tr_logger_dout, '022b'))


################################################################
### B-channel commands
################################################################
    
    def send_ipb_BCmd(self, bcmd):
        print ('Sending BCmd:' + str(hex(bcmd)))
        self.ipb_write("user.ctrl_regs.ChB.ipb_BCmd",bcmd,1)

    def send_ipb_BX(self, bx):
        print ('Sending BX:' + str(hex(bx)))
        self.ipb_write("user.ctrl_regs.ChB.ipb_BX",bx,1)

    def bc0_enable(self):
        self.ipb_write("user.ctrl_regs.BC0.bc0_enable",1,1)
        print("bc0 enabled")

    def bc0_disable(self):
        self.ipb_write("user.ctrl_regs.BC0.bc0_enable",0,1)
        print("bc0_disabled")
        
    def set_abort_gap(self, nbx):
        print('Setting abort gap to start at bcid: ' + str(nbx))
        self.ipb_write("user.ctrl_regs.BC0.abort_gap",nbx,1)
        
    def set_abort_gap_end(self, nbx):
        print('Setting abort gap to end before bcid: ' + str(nbx))
        self.ipb_write("user.ctrl_regs.trigger2.abort_gap_end",nbx,1)
        
    def set_orbit_length(self, nbx):
        print ('Setting orbit length to: ' + str(nbx))
        self.ipb_write("user.ctrl_regs.ChB_proc.Orbit_length",nbx,1)

    def set_bc0_command(self, bc0_command):
        print ('Setting Beginning-of-orbit (aka BC0) command to:' + str(hex(b0_command)))
        self.ipb_write("user.ctrl_regs.BC0.bc0_command",bc0_command,1)

    def send_ChB_proc_sendahead_time(self, sendahead_time):
        print ('Sending sendahead time:' + str(hex(sendahead_time)))
        self.ipb_write("user.ctrl_regs.ChB_proc.ChB_proc_sendahead_time",sendahead_time,1)

    def send_ChB_proc_clk_sync_del(self, clk_sync_del):
        print ('Sending ChB_proc_clk_sync_del:' + str(hex(clk_sync_del)))
        self.ipb_write("user.ctrl_regs.ChB_proc.ChB_proc_clk_sync_del",clk_sync_del,1)

    def send_BXCounter_clk_sync_del(self, clk_sync_del):
        print ('Sending BXCounter_clk_sync_del:' + str(hex(clk_sync_del)))
        self.ipb_write("user.ctrl_regs.ChB_proc.BXCounter_clk_sync_del",clk_sync_del,1)

    def send_BXCounter_update_slot(self, update_slot):
        print ('Sending BXCounter_update_slot:' + str(hex(update_slot)))
        self.ipb_write("user.ctrl_regs.ChB_proc.BXCounter_update_slot",update_slot,1)

    def ChB_busy(self):
        ChB_busy = self.ipb_read("user.stat_regs.ChB_proc.ChB_busy",1)
        print ("------------------------------------------------")
        print ("Channel B busy", ChB_busy)
        print ("------------------------------------------------")
        print ("")

    def bx_counter_initialized(self):
        bx_counter_initialized = self.ipb_read("user.stat_regs.ChB_proc.bx_counter_initialized",1)
        print ("------------------------------------------------")
        print ("BX counter initialized ", bx_counter_initialized)
        print ("------------------------------------------------")
        print ("")

    def transistion_logger_reset_up(self):
        self.ipb_write("user.ctrl_regs.ChB_proc.Transition_logger_reset",1,1)
        print("Transition logger reset high")

    def transistion_logger_reset_down(self):
        self.ipb_write("user.ctrl_regs.ChB_proc.Transition_logger_reset",0,1)
        print("Transition logger reset low")

    def status_cnt(self):
        evcnt = self.ipb_read("user.stat_regs.counter_1.evcnt",1)
        bcnt = self.ipb_read("user.stat_regs.counter_1.bcnt",1)
        orbcnt = self.ipb_read("user.stat_regs.counter_2.orbcnt",1)
        evrcnt = self.ipb_read("user.stat_regs.counter_2.evrcnt",1)
        print ("Counter values ...")
        print ("------------------------------------------------")
        print ("event counter             ", evcnt)
        print ("bunch crossing counter    ", bcnt)
        print ("orbit counter             ", orbcnt)
        print ("event reject counter      ", evrcnt)
        print ("------------------------------------------------")
        print ("")

    def status_orb(self):
        abort_gap = self.ipb_read("user.ctrl_regs.BC0.abort_gap",1)
        abort_gap_end = self.ipb_read("user.ctrl_regs.trigger2.abort_gap_end",1)
        orbit_length = self.ipb_read("user.ctrl_regs.ChB_proc.Orbit_length",1)
        print ("Orbit configuration ...")
        print ("------------------------------------------------")
        print ("abort gap from                ", abort_gap)
        print ("abort gap to                ", abort_gap_end)
        print ("orbit length              ", orbit_length)
        print ("------------------------------------------------")
        print ("")

################################################################
### Asynchronous trigger generator
################################################################
    
    def set_async_trigger_enable(self, bit):
        self.ipb_write("user.ctrl_regs.trigger.async_trigger_enable",bit,1)

    def enable_async_trigger(self):
        self.set_async_trigger_enable(1)

    def disable_async_trigger(self):
        self.set_async_trigger_enable(0)

    def read_async_trigger_enable_status(self):
        bit = self.ipb_read("user.ctrl_regs.trigger.async_trigger_enable",1)
        print ("async_trigger enable status", bit)
        return bit

    def set_async_trigger_fine_time_adjust(self, fine_time_adjust):
        self.ipb_write("user.ctrl_regs.async_trigger_2.fine_time_adjust",fine_time_adjust,1)
	
    def read_async_trigger_fine_time_adjust(self):
        outtap = self.ipb_read("user.stat_regs.async_trigger.fine_time_adjust",1)
        print ("async_trigger.fine_time_adjust", outtap)
        return outtap

    def set_async_trigger_coarse_time_adjust(self, coarse_time_adjust):
        self.ipb_write("user.ctrl_regs.async_trigger.coarse_time_adjust",coarse_time_adjust,1)

    def read_async_trigger_coarse_time_adjust(self):
        coarse_time_adjust = self.ipb_read("user.ctrl_regs.async_trigger.coarse_time_adjust",1)
        print ("async trigger coarse_time_adjust", coarse_time_adjust)
        return coarse_time_adjust

    def set_async_trigger_spacing(self, trigger_spacing):
        self.ipb_write("user.ctrl_regs.async_trigger.trigger_spacing",trigger_spacing,1)

    def read_async_trigger_spacing(self):
        trigger_spacing = self.ipb_read("user.ctrl_regs.async_trigger.trigger_spacing",1)
        print ("async trigger trigger_spacing   ", trigger_spacing)
        return trigger_spacing

    def set_async_trigger_num_triggers(self, num_triggers):
        self.ipb_write("user.ctrl_regs.async_trigger.num_triggers",num_triggers,1)

    def read_async_trigger_num_triggers(self):
        num_triggers = self.ipb_read("user.ctrl_regs.async_trigger.num_triggers",1)
        print ("async trigger num_triggers   ", num_triggers)
        return num_triggers


################################################################
### Trigger phase measurement
################################################################

## new, for L1a phase version 1, the default:

    def set_l1a_phase_v1_phase_offset(self, value):
        self.ipb_write("user.ctrl_regs.l1a_phase_v1.phase_offset",value,1)

    def set_l1a_phase_v1_acceptance_window_from(self, value):
        self.ipb_write("user.ctrl_regs.l1a_phase_v1.acceptance_window_from",value,1)

    def read_l1a_phase_v1_acceptance_window_from(self):
        value=self.ipb_read("user.ctrl_regs.l1a_phase_v1.acceptance_window_from",1)
        return value

    def set_l1a_phase_v1_acceptance_window_to(self, value):
        self.ipb_write("user.ctrl_regs.l1a_phase_v1.acceptance_window_to",value,1)

    def read_l1a_phase_v1_acceptance_window_to(self):
        value=self.ipb_read("user.ctrl_regs.l1a_phase_v1.acceptance_window_to",1)
        return value

    def read_trigger_acceptance_window(self):
        val_from = self.read_l1a_phase_v1_acceptance_window_from()
        val_to = self.read_l1a_phase_v1_acceptance_window_to()
        bin_width=1.25 
        print("Trigger acceptance window set between:", val_from, val_to) 
        print("                                     :", val_from*bin_width, val_to*bin_width) 

    def set_l1a_phase_v1_clear(self, bit):
        self.ipb_write("user.ctrl_regs.l1a_phase_v1.clear",bit,1)

    def set_l1a_phase_v1_pll_reset(self):
        self.ipb_write("user.ctrl_regs.l1a_phase_v1.pll_reset",1,1)
        sleep(1)
        self.ipb_write("user.ctrl_regs.l1a_phase_v1.pll_reset",0,1)


    def read_l1a_phase_v1_phase(self):
        val = self.ipb_read("user.stat_regs.l1a_phase_v1.phase",1)
        return val

    def read_l1a_phase_v1_trg_flag_del(self):
        val = self.ipb_read("user.stat_regs.l1a_phase_v1.trg_flag_delayed_p5ns",1)
        return val

    def read_l1a_phase_v1_pll_locked(self):
        val = self.ipb_read("user.stat_regs.l1a_phase_v1.pll_locked",1)
        return val
    
    def l1a_phase_v1(self):
        val = self.read_l1a_phase_v1_phase()
        print ("L1A phase: ", val, hex(val))
        return val

    
### This is now for the old version 2

    def set_l1a_phase_v2_clear(self, bit):
        self.ipb_write("user.ctrl_regs.l1a_phase.clear",bit,1)

    def clear_l1a_phase_v2(self):
        # Clearing last measurement value
        self.set_l1a_phase_v2_clear(1)
        sleep(0.5)
        self.set_l1a_phase_v2_clear(0)


    def set_l1a_phase_v2_select(self, select):
        self.ipb_write("user.ctrl_regs.l1a_phase.select",select,1)

    def set_l1a_phase_v2_internal_clock_counter_offset(self, offset):
        self.ipb_write("user.ctrl_regs.l1a_phase.internal_clock_counter_offset",offset,1)

    def set_l1a_phase_v2_self_calibrate(self, bit):
        self.ipb_write("user.ctrl_regs.l1a_phase.self_calibrate",bit,1)

    def set_l1a_phase_v2_coarse_phase_adjust_input(self, value):
        self.ipb_write("user.ctrl_regs.l1a_phase.coarse_phase_adjust_input",value,1)

    def read_l1a_phase_v2_coarse_phase_adjust_input(self):
        return self.ipb_read("user.ctrl_regs.l1a_phase.coarse_phase_adjust_input",1)

    def set_l1a_phase_v2_fine_phase_adjust_input(self, value):
        self.ipb_write("user.ctrl_regs.l1a_phase.fine_phase_adjust_input",value,1)

    def read_l1a_phase_v2_fine_phase_adjust_input(self):
        return self.ipb_read("user.ctrl_regs.l1a_phase.fine_phase_adjust_input",1)

    def set_l1a_phase_v2_inv(self, bit):
        self.ipb_write("user.ctrl_regs.l1a_phase.inv",bit,1)


    def read_l1a_phase_v2_reg(self):
        val = self.ipb_read("user.stat_regs.l1a_phase.phase",1)
        return val

    def read_l1a_phase_v2(self):
        val = self.read_l1a_phase_v2_reg()
        lsbs = 2**self.read_l1a_phase_v2_fine_phase_width()
        val_coarse = val//lsbs
        val_fine = val%lsbs
        val_nbins = self.read_l1a_phase_v2_fine_phase_granularity()
        val = val_coarse*val_nbins + val_fine
        return val

    def l1a_phase_v2(self):
        val = self.read_l1a_phase_v2()
        print ("L1Aph_phase:", val, hex(val))
        return val


    def read_l1a_phase_v2_calibration_status(self):
        calibration_done = self.ipb_read("user.stat_regs.l1a_phase.calibration_done",1)
        #print "Self-calibration success:", calibration_done
        return calibration_done

    def read_l1a_phase_v2_fine_phase_adjust(self):
        # reading out fine_phase_adjust
        fine_phase_adjust = self.ipb_read("user.stat_regs.l1a_phase.fine_phase_adjust_output",1)
        #print "fine_phase_adjust_output    ", fine_phase_adjust
        return fine_phase_adjust

    def read_l1a_phase_v2_coarse_phase_adjust(self):
        # reading out coarse time adjust
        coarse_phase_adjust_output = self.ipb_read("user.stat_regs.l1a_phase.coarse_phase_adjust_output",1)
        #print "coarse_phase_adjust_output   ", coarse_phase_adjust_output
        return coarse_phase_adjust_output

    def read_l1a_phase_v2_fine_phase_width(self):
        val = self.ipb_read("user.stat_regs.l1a_phase.fine_phase_width",1)
        return val

    def read_l1a_phase_v2_fine_phase_granularity(self):
        val = self.ipb_read("user.stat_regs.l1a_phase.fine_phase_granularity",1)
        return val

    def read_l1a_phase_v2_fine_phase_bin_size(self):
        val = self.ipb_read("user.stat_regs.l1a_phase.fine_phase_bin_size",1)
        return val

    def read_l1a_phase_v2_measurement_monitor32_reg(self):
        mon32 = self.ipb_read("user.stat_regs.l1a_phase_monitor32",1)
        return mon32

    def read_l1a_phase_v2_measurement_monitor32(self):
        mon32 = self.ipb_read("user.stat_regs.l1a_phase_monitor32",1)
        val_mon32 = str('{:032b}'.format(mon32))
        val_nbins = self.read_l1a_phase_v2_fine_phase_granularity()
        val_ret = val_mon32[-val_nbins:]
        return val_ret

    def read_l1a_phase_v2_select(self):
        L1Aph_select = self.ipb_read("user.ctrl_regs.l1a_phase.select",1)
        return L1Aph_select
        #print "------------------------------------------------"
        #print "L1Aph_select", L1Aph_select
        #print "------------------------------------------------"
        #print ""


################################################################
################################################################
################################################################

