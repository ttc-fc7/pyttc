#!/usr/bin/env python
#----------------------------------------------------------------------------------
# file ttc.py
#----------------------------------------------------------------------------------
# author : Viktor Veszpremi (viktor.veszpremi@cern.ch)
# contributions from:
#          Tamas Balazs (tamas.balazs@cern.ch)
#          Lajos Palanki (LP618@ic.ac.uk)
#          Alexander Ruede (aruede@cern.ch)
#----------------------------------------------------------------------------------
# version 1.0
#
# Details :
# TTC-FC7 firmware python interface
#
# Last changes:
# 2021.09.07
#---------------------------------------------------------------------------------

import sys
from pyTTC import *
from time import sleep
import math

uTTC = pyTTC( "192.168.4.20" )
sleep(0.2)


################################################################
### HELP
################################################################

if (len(sys.argv) > 1 and sys.argv[1] == "help") or (len(sys.argv)==1):
    print ("Main commands")
    print (" ")
    print ("    reset\t : command issues general reset")
    print ("    configure\t : configure TTC-FC7 for running")
    print (" ")
    print ("Main command groups")
    print ("    cnc\t\t: clock and counters")
    print ("    fmc\t\t: configure fmc")
    print ("    dio5\t\t: configure dio5")
    print ("    sfp+\t\t: confgure laser driver")
    print ("    bpl\t\t: confgure backplane connections")
    print ("    trg\t\t: configure and send triggers")
    print ("    phs\t\t: calibrate and read trigger phase measurement")
    print ("    orb\t\t: configure orbit")    
    print ("    bgo\t\t: send TTC commands to crate")
    print ("    chb\t\t: <DEBUG> send individual b-channel commands")
    print ("    log\t\t: read state transition logger")
    print ("    tts\t\t: read TTS state and test TTS FSM")
    print (" ")
    print ("Note: running command group without a command prints")
    print ("out a help for the command group ")
    print (" ")


################################################################
### Configure hardware
################################################################

# General reset of the user_core
if len(sys.argv) > 1 and sys.argv[1] == "reset":
    uTTC.reset_setup()


if len(sys.argv) > 1 and sys.argv[1] == "configure":
    uTTC.fmc_pwr_on()
    uTTC.i2c_enable()
    uTTC.i2c_command()
    uTTC.set_io_DIO5(9) # OUT(TRGEMU)-IN(TRG)-IN(TRGINHIBIT)-OUT(TRG)-IN(CLK)
    uTTC.enable_load_config_DIO5()
    uTTC.set_channel_threshold_DIO5(1, 75) # set to 1.1V
    uTTC.set_channel_threshold_DIO5(2, 75) # set to 1.1V
    uTTC.set_channel_threshold_DIO5(3, 75) # set to 1.1V
    uTTC.set_channel_threshold_DIO5(4, 75) # set to 1.1V
    uTTC.set_channel_threshold_DIO5(5, 75) # set to 1.1V
    uTTC.disable_load_config_DIO5()
    uTTC.enable_DIO5()


# CLOCK AND COUNTER
# -----------------

if len(sys.argv) == 2 and sys.argv[1] == "cnc":
    print ("Commands")
    print ("    clk_rate\t : print out clock rate measurements")
    print ("    cnt\t : print out incidental values of counters")
    print ("    set ...")
    print ("        clk\t: 0: on-board clock source, 1: external clock")
    print (" ")
    
#  Reading clock rate tool status
if len(sys.argv) > 2 and sys.argv[1] == "cnc" and sys.argv[2] == "clk_rate":
    uTTC.status_crt()

#  Reading counter values
if len(sys.argv) > 2 and sys.argv[1] == "cnc" and sys.argv[2] == "cnt":
    uTTC.status_cnt()

if len(sys.argv) > 3 and sys.argv[1] == "cnc" and sys.argv[2] == "set" and sys.argv[3] == "clk":

    if sys.argv[4] == "0":
        # default onboard clock selected by system core
        uTTC.clock_source(3)
        # by default, clock generator uses the fabric clock (not external)
        uTTC.ext_clk(0)
    elif sys.argv[4] == "1":
        # external clock from FMC selected by system core
        uTTC.clock_source(1)
        # explicitely disabling switching to external clock in the clock generator (already done)
        uTTC.ext_clk(0)    
    sleep(0.2)
    uTTC.reset_setup()
    sleep(0.2)
    uTTC.status_clk_gen()

if len(sys.argv) > 2 and sys.argv[1] == "BXcounter_clk_sync_del":	#  2 bits
    uTTC.send_BXcounter_clk_sync_del(int(sys.argv[2]))

if len(sys.argv) > 2 and sys.argv[1] == "BXcounter_update_slot":	#  3 bits
    uTTC.send_BXcounter_update_slot(int(sys.argv[2]))

if len(sys.argv) > 1 and sys.argv[1] == "bx_counter_initialized":
    uTTC.bx_counter_initialized()



# FMC POWERING
# ------------

if len(sys.argv) == 2 and sys.argv[1] == "fmc":
    print ("Commands")
    print ("    on \t\t: turns power ON for both L8 and L12 FMC")
    print ("    off \t: turns power OFF for both L8 and L12 FMC")
    print ("    set_as_trigger_source\t: makes FMC L12 spare pin 7 the source of external trigger")
    print (" ")
    print ("Expert commands")
    print ("    l12_pwr_on \t: turns power ON for L12 FMC")
    print ("    l8_pwr_on \t: turns power ON for L8 FMC")
    print (" ")
    print ("Note: fmc power on is called during configure, no need to call it again")
    print (" ")
    
if len(sys.argv) > 2 and sys.argv[1] == "fmc" and sys.argv[2] == "l12_pwr_on":
    uTTC.l12_pwr_on()

if len(sys.argv) > 2 and sys.argv[1] == "fmc" and sys.argv[2] == "l8_pwr_on":
    uTTC.l8_pwr_on()

if len(sys.argv) > 2 and sys.argv[1] == "fmc" and sys.argv[2] == "on":
    uTTC.fmc_pwr_on()

if len(sys.argv) > 2 and sys.argv[1] == "fmc" and sys.argv[2] == "off":
    uTTC.fmc_pwr_off()

if len(sys.argv) > 2 and sys.argv[1] == "fmc" and sys.argv[2] == "set_as_trigger_source":
    uTTC.set_external_trigger_from_fmc_l12_spare_pin7()


# DIO5
# -----

if len(sys.argv) == 2 and sys.argv[1] == "dio5":
    print ("Commands")
    print (" ")
    print ("    info \t\t: prints out the current channel settings")
    print (" ")
    print ("    set_as_trigger_source\t: makes dio5 channel 2 the source of external trigger")
    print (" ")
    print ("    set ...")
    print ("        term <ch> <state>\t : set 50 Ohm input termination on channel ch [1..5], state: 1 - enabled or 0 - disabled")
    print ("        thr <ch> <val>\t : set input threshold of val [0..255] on channel ch [1..5] (NOTE: direction of channel is not checked!!!)")
    print ("        thr_V <ch> <val>\t : set input threshold between 0.16V and 3.4V on channel ch [1..5] (NOTE: direction of channel is not checked!!!)")
    print (" ")
    print ("    read ...")
    print ("        term <ch>\t : read 50 Ohm input termination on channel ch [1..5], state: 1 - enabled or 0 - disabled")
    print ("        thr <ch>\t : read input threshold on channel ch [1..5] (NOTE: direction of channel is not checked!!!)")
    print (" ")
    print ("Note: dio5 configuration is called during main configure, no need to call it again")
    print (" ")
    print ("Expert commands -- IMPORTANT: driving a signal into an output channel is not a good idea!")
    print ("")
    print ("    set ...")
    print ("        io <ch> <IN/OUT>\t : change direction (function) of channel ch (currently only 3)")
    print ("                     \t   ch=3: OUT - output fabric clock / IN - input trigger inhibit")
    print ("")

    
if len(sys.argv) > 2 and sys.argv[1] == "dio5" and sys.argv[2] == "info":
    print ("CH\tDir\tThr\tTerm")
    for i in range (1, 6):
        print ("", i, "\t", uTTC.read_channel_io_DIO5(i), "\t", uTTC.read_channel_threshold_DIO5(i),
               "\t", uTTC.read_channel_term_50_DIO5(i))

if len(sys.argv) > 2 and sys.argv[1] == "dio5" and sys.argv[2] == "set_as_trigger_source":
    uTTC.set_external_trigger_from_dio5()

if len(sys.argv) > 5 and sys.argv[1] == "dio5" and sys.argv[2] == "set" and sys.argv[3] == "term":
    uTTC.enable_load_config_DIO5()
    uTTC.set_channel_term_50_DIO5(int(sys.argv[4]), int(sys.argv[5]))
    uTTC.disable_load_config_DIO5()

if len(sys.argv) > 4 and sys.argv[1] == "dio5" and sys.argv[2] == "read" and sys.argv[3] == "term":
    print (uTTC.read_channel_term_50_DIO5(int(sys.argv[4])))

if len(sys.argv) > 5 and sys.argv[1] == "dio5" and sys.argv[2] == "set" and sys.argv[3] == "thr":
    uTTC.enable_load_config_DIO5()
    uTTC.set_channel_threshold_DIO5(int(sys.argv[4]), int(sys.argv[5]))
    uTTC.disable_load_config_DIO5()

if len(sys.argv) > 5 and sys.argv[1] == "dio5" and sys.argv[2] == "set" and sys.argv[3] == "thr_V":
    uTTC.enable_load_config_DIO5()
    if float(sys.argv[5]) <0.157:
        val = 0.157
    else:
        val = math.ceil((float(sys.argv[5])-0.157)/0.0126)
    if val > 255:
        val = 255
    print ("Setting for thershold:", int(val))
    uTTC.set_channel_threshold_DIO5(int(sys.argv[4]), int(val))
    uTTC.disable_load_config_DIO5()

if len(sys.argv) > 5 and sys.argv[1] == "dio5" and sys.argv[2] == "set" and sys.argv[3] == "io":
    uTTC.set_channel_io_DIO5(int(sys.argv[4]), sys.argv[5])
    

if len(sys.argv) > 4 and sys.argv[1] == "dio5" and sys.argv[2] == "read" and sys.argv[3] == "thr":
    print (uTTC.read_channel_threshold_DIO5(int(sys.argv[4])))


# SFP+
# ----

if len(sys.argv) == 2 and sys.argv[1] == "sfp+":
    print ("Commands")
    print ("    configure\t\t: configures the 8-SFP+ FMC cage and SFP+ module")
    print ("    set_as_trigger_source\t: makes SFP+ A RX the source of external trigger")
    print (" ")
    print ("Note: sfp+ configuration is called during main configure, no need to call it again")
    print (" ")


# 8SFP: program I2C extension in order to access the enable line of the SFPs and enable all SFPs
if len(sys.argv) > 1 and sys.argv[1] == "prog_i2c":
    uTTC.i2c_enable()
    uTTC.i2c_command()

if len(sys.argv) > 2 and sys.argv[1] == "sfp+" and sys.argv[2] == "set_as_trigger_source":
    uTTC.set_external_trigger_from_sfp()
    

# Backplane
# ---------

if len(sys.argv) == 2 and sys.argv[1] == "bpl":
    print ("Commands")
    print ("    set_as_trigger_source\t: makes the backplane the source of external trigger")
    print (" ")

if len(sys.argv) > 2 and sys.argv[1] == "bpl" and sys.argv[2] == "set_as_trigger_source":
    uTTC.set_external_trigger_from_backplane()
    

################################################################
### Trigger
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "trg":
    print ("Trigger commands")
    print ("NOTE: the TTC-FC7 is designed to process externally generated triggers. The external trigger input source is ")
    print ("selected by commands under the relevant interfaces: dio5, fmc, sfp+, bpl (NOT HERE!)")
    print ("If an external trigger source is not available, the TTC-FC7 can generate or route triggers from its various interfaces.")
    print ("Such self-generated triggers need to be physically looped back to the active external input from one of the")
    print ("available output sources (see 'set src' command below).")
    print (" ")
    print ("General commands")
    print ("    status : print out active external and generated trigger sources, enabled/disabled state and rules applied")
    print ("     start : enable triggers to be sent to the AMC13 (and STARTs triggering immediately if triggers are incoming)")
    print ("      stop : disable triggers to be sent to the AMC13 (and STOPs trigger even if triggers are incoming")
    print (" ")
    print ("    set ...")
    print ("        window <0..19> <0..19>\t: set the first and last bin of the trigger acceptance window")
    print ("        latency <0..4095> : set the trigger latency in number of bunch-crossings")
    print ("        rule <id> : set trigger rule bits <IT><PS><2S><CMS> with a 4-bit word")    
    print ("        prescale <n> : pseudo-random prescale by 1/n where n < 2*10^7")
    print ("        request <n> : set request for exactly <n> number of L1A-s to be emitted")
    print ("                      start releasing triggers with 'start' and exit this mode with 'stop'")
    print (" ")
    print ("    read ...")
    print ("        avg_rate : prints out the average trigger rate in the last 1.5 seconds")
    print (" ")
    print ("Self-generated trigger output sources")
    print ("NOTE: all self-generated triggers are outputted in all the following interfaces simultaneously: ")
    print ("      DIO5 CH1, SFP+ B TX, FMC L12 spare pin 6. As noted, they need to be looped-back for use.")
    print (" ")
    print ("    set ...")
    print ("        src <id> :  set source for self-generated triggers (see NOTEs in this page)")
    print ("                 :  <id> is 1 - ipbus, 6 - async trigger, 7 - sync clocked (0 - no trigger)")
    print (" ")
    print ("    ipbus ...")
    print ("         send <number of triggers> : send a number of (random) ipbus triggers")
    print (" ")
    print ("    sync ...")
    print ("         start <interval> : starts periodic clocked trigger with fixed interval in bx")
    print ("         stop : stop periodic clocked trigger")
    print (" ")
    print ("    async ...")
    print ("         enable : enable output and start emitting defined number of triggers or infinite many if 0 was requested")
    print ("                   (important: configuration of async trigger generator should happen during disabled state)")
    print ("         disable : stop triggers and disable output. Disabling is required to be able to run enable again")
    print ("                   (e.g. a disable+enable cycle re-emits a pre-configured number of triggers)")
    print ("         send <number of triggers> <spacing> <coarse phase> <fine phase> : send out a number of triggers with")
    print ("                                                                           bx-spacing and relative phase to LHC clock")
    print ("         stat : print out active configuration parameters")
    print (" ")
    print ("Expert commands")
    print (" ")
    print ("    fifo_cnt : print out the number of L1A-s in the L1A FIFO (waiting for their latency countdown)")
    print (" ")
    print ("    set ...")
    print ("        prescale_cut <val>:  set the prescale cut")
    print ("        src <id> :  set source for self-generated triggers")
    print ("                 :  <id> is 2 - SFP+ B, 3 - fmc L12 spare pin 7, 4 - backplane, 5 - DIO5 CH2")
    print (" ")
    print ("    read ...")
    print ("        prescale_cut:  read the prescale cuts")
    print (" ")
    

# Trigger sources, rules, stat
# ----------------------------

if len(sys.argv) > 2 and sys.argv[1] == "trg" and sys.argv[2] == "status":
    uTTC.read_external_trigger_select()
    uTTC.read_trigger_enable_state()
    uTTC.read_trigger_acceptance_window()
    uTTC.read_trigger_latency()
    uTTC.read_trigger_rule()
    uTTC.read_trigger_source()
    if (uTTC.read_l1a_prescale_cut() != 0):
        print("Prescale set to:", round(1/(1-uTTC.read_l1a_prescale_cut()/2147483647)))
    else:
        print("Prescale set to: 1")
    if (uTTC.read_l1a_count_request() != 0):
        print("!!! Number of L1A requested:", uTTC.read_l1a_count_request())
        print("    ...exit this mode with 'stop'")

if len(sys.argv) > 2 and sys.argv[1] == "trg" and sys.argv[2] == "start":
    if (uTTC.read_l1a_count_request() != 0):
        print("Start releasing number of triggers: ", uTTC.read_l1a_count_request())
        print("!!! Remember to stop triggering to exit this triggering mode !!!")
        print("!!! ...otherwise all other triggers will be L1R... !!!")
    uTTC.enable_triggers();

if len(sys.argv) > 2 and sys.argv[1] == "trg" and sys.argv[2] == "stop":
    if (uTTC.read_l1a_count_request() != 0):
        print("Exiting counter L1A release mode...")
        uTTC.set_l1a_count_request_en(0)
        uTTC.set_l1a_count_request(0)
    uTTC.disable_triggers();

if len(sys.argv) > 5 and sys.argv[1] == "trg" and sys.argv[2] == "set" and sys.argv[3] == "window":
    uTTC.set_l1a_phase_v1_acceptance_window_from(int(sys.argv[4]))
    uTTC.set_l1a_phase_v1_acceptance_window_to(int(sys.argv[5]))

if len(sys.argv) > 4 and sys.argv[1] == "trg" and sys.argv[2] == "set" and sys.argv[3] == "latency":
    uTTC.set_trigger_latency(int(sys.argv[4]))

if len(sys.argv) > 4 and sys.argv[1] == "trg"  and sys.argv[2] == "set" and sys.argv[3] == "rule":
    uTTC.set_trigger_rule(int(sys.argv[4]))

if len(sys.argv) > 4 and sys.argv[1] == "trg"  and sys.argv[2] == "set" and sys.argv[3] == "prescale":
    n = int(sys.argv[4])
    cut=round((2147483647/n)*(n-1))
    n=round(1/(1-cut/2147483647))
    uTTC.set_l1a_prescale_cut(cut)        
    print("Setting scale factor to: ", n)
        
if len(sys.argv) > 4 and sys.argv[1] == "trg"  and sys.argv[2] == "set" and sys.argv[3] == "prescale_cut":
    uTTC.set_l1a_prescale_cut(int(sys.argv[4]))
    
if len(sys.argv) > 3 and sys.argv[1] == "trg"  and sys.argv[2] == "read" and sys.argv[3] == "prescale_cut":
    print ("The prescale cut value for PRNG is", uTTC.read_l1a_prescale_cut())

if len(sys.argv) > 3 and sys.argv[1] == "trg" and sys.argv[2] == "read" and sys.argv[3] == "avg_rate":
    print ("The average trigger rate (updates every 1.5 sec) is", uTTC.read_l1a_avg_rate(), "kHz")

if len(sys.argv) > 4 and sys.argv[1] == "trg"  and sys.argv[2] == "set" and sys.argv[3] == "src":
    uTTC.set_trigger_source(int(sys.argv[4]))
    ##uTTC.enable_triggers() ### added for external trigger
    uTTC.read_trigger_source()

if len(sys.argv) > 4 and sys.argv[1] == "trg"  and sys.argv[2] == "set" and sys.argv[3] == "request":
    if (uTTC.read_l1a_count_request_en() == 1):
        print("!!! L1A triggering is active or has been completed but not stopped !!!")
        print("Stop triggers before requesting a new batch")
    else:
        n = int(sys.argv[4])
        uTTC.set_l1a_count_request(n)
        uTTC.set_l1a_count_request_en(1)
        print("Setting number of requested L1A-s to: ", n)
        print("Triggers will be released upon 'start', then do not forget to issue 'stop'")


# IPbus triggers
# --------------

if len(sys.argv) > 4 and sys.argv[1] == "trg" and sys.argv[2] == "ipbus" and sys.argv[3] == "send":
    uTTC.set_trigger_source(1)
    uTTC.enable_triggers()
    uTTC.send_ipbus_trigger(0.1,int(sys.argv[4]),0.001)
    uTTC.disable_triggers()


# Periodic clocked triggers
# -------------------------

if len(sys.argv) > 4 and sys.argv[1] == "trg" and sys.argv[2] == "sync" and sys.argv[3] == "start":
    uTTC.set_trigger_source(7)
    uTTC.set_trigger_interval(int(sys.argv[4]))
    uTTC.enable_triggers()
    uTTC.read_trigger_source()
    uTTC.read_trigger_enable_state()

if len(sys.argv) > 3 and sys.argv[1] == "trg" and sys.argv[2] == "sync" and sys.argv[3] == "stop":
    uTTC.set_trigger_source(0)
    uTTC.disable_triggers()
    uTTC.read_trigger_source()
    uTTC.read_trigger_enable_state()


# Asynchronous trigger
# --------------------

if len(sys.argv) == 3 and sys.argv[1] == "trg" and sys.argv[2] == "async":
    print ("Asynchronous Trigger commands")
    print ("  trg async ...")
    print ("         enable : enable output and start emitting defined number of triggers or infinite many if 0 was requested")
    print ("                   (important: configuration of async trigger generator should happen during disabled state)")
    print ("         disable : stop triggers and disable output. Disabling is required to be able to run enable again")
    print ("                   (e.g. a disable+enable cycle re-emits a pre-configured number of triggers)")
    print ("         send <number of triggers> <spacing> <coarse phase> <fine phase> : send out a number of triggers with")
    print ("                                                                           bx-spacing and relative phase to LHC clock")
    print ("         stat : print out active configuration parameters")
    

if len(sys.argv) > 3 and sys.argv[1] == "trg" and sys.argv[2] == "async" and sys.argv[3] == "enable":
    uTTC.enable_async_trigger()
    uTTC.read_async_trigger_enable_status()

if len(sys.argv) > 3 and sys.argv[1] == "trg" and sys.argv[2] == "async" and sys.argv[3] == "disable":
    uTTC.disable_async_trigger()
    uTTC.read_async_trigger_enable_status()

if len(sys.argv) > 7 and sys.argv[1] == "trg" and sys.argv[2] == "async" and sys.argv[3] == "send":
    uTTC.set_trigger_rule(0)
    uTTC.set_trigger_source(6)
    uTTC.enable_triggers()
    uTTC.disable_async_trigger()
    uTTC.set_async_trigger_num_triggers(int(sys.argv[4]))
    uTTC.read_async_trigger_num_triggers()
    uTTC.set_async_trigger_spacing(int(sys.argv[5]))
    uTTC.read_async_trigger_spacing()
    uTTC.set_async_trigger_coarse_time_adjust(int(sys.argv[6]))
    uTTC.read_async_trigger_coarse_time_adjust()
    uTTC.set_async_trigger_fine_time_adjust(int(sys.argv[7]))
    uTTC.read_async_trigger_fine_time_adjust()
    sleep(0.1)
    uTTC.enable_async_trigger()
    sleep(0.1)
    uTTC.disable_async_trigger()


if len(sys.argv) > 3 and sys.argv[1] == "trg" and sys.argv[2] == "async" and sys.argv[3] == "stat":
    print ("Current async trigger generator settings:")
    uTTC.read_async_trigger_enable_status()
    uTTC.read_async_trigger_coarse_time_adjust()
    uTTC.read_async_trigger_spacing()
    uTTC.read_async_trigger_num_triggers()
    uTTC.read_async_trigger_fine_time_adjust()


# Simple trigger monitor (if it still works)
# -----------------------------------------

if len(sys.argv) > 1 and sys.argv[1] == "monitor_fifo":
    uTTC.check_FIFO()


if len(sys.argv) > 2 and sys.argv[1] == "trg" and sys.argv[2] == "fifo_cnt":
    uTTC.read_l1a_fifo_cnt()


################################################################
### Trigger phase measurement
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "phs":
    print ("Commands")
    print ("   trg\t\t\t\t: print out measured phase of the running trigger")
    print ("   enable\t\t\t\t\t: allow measurement to update after every trigger")
    print ("   disable\t\t\t\t\t: force the measured value to 0")
    print ("   reset_pll\t\t\t\t\t: reset the pll inside the phase measurement module")
    print (" ")
    print ("Calibration")
    print ("   set ...")
    print ("       offset <0..19>\t: shift the measured phase by this value if synchronous trigger is not measured in bin 0 (default is 0)")
    print (" ")
    print ("Expert commands")
    print ("   read ...")
    print ("       pll\t\t: the PLL lock state")
    print ("       trg_flag_del\t\t: L1A output from phase measurement module with 5ns delay")
    print (" ")


if len(sys.argv) > 2 and sys.argv[1] == "phs" and sys.argv[2] == "trg":
    uTTC.l1a_phase_v1()

if len(sys.argv) > 2 and sys.argv[1] == "phs" and sys.argv[2] == "enable":
    uTTC.set_l1a_phase_v1_clear(0)

if len(sys.argv) > 2 and sys.argv[1] == "phs" and sys.argv[2] == "disable":
    uTTC.set_l1a_phase_v1_clear(1)

if len(sys.argv) > 2 and sys.argv[1] == "phs" and sys.argv[2] == "reset_pll":
    uTTC.set_l1a_phase_v1_pll_reset()

if len(sys.argv) > 4 and sys.argv[1] == "phs" and sys.argv[2] == "set" and sys.argv[3] == "offset":
    uTTC.set_l1a_phase_v1_phase_offset(int(sys.argv[4]))

if len(sys.argv) > 3 and sys.argv[1] == "phs" and sys.argv[2] == "trg" and sys.argv[3] == "pll":
    print("PLL lock state: ", uTTC.read_l1a_phase_v1_pll_locked())

if len(sys.argv) > 3 and sys.argv[1] == "phs" and sys.argv[2] == "trg" and sys.argv[3] == "trg_flag_del":
    print("trg_flag_del: ", uTTC.read_l1a_phase_v1_trg_flag_del())
    

    
################################################################
### Trigger phase measurement for version 2 (not used)
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "phs2":
    print ("Commands")
    print ("   load_settings\t: load a predetermined set of coarse/fine adjust settings into input registers")
    print ("   dump_settings\t: dump content of coarse/fine adjust settings from input registers")
    print ("   configure\t: configure module using coare/fine adjust settings in the input registers (invalidates self-calibration)")
    print ("   calibrate dio5/sfp+/pin7: run self-calibration in loopback mode (replaces settings loaded from input registers)")
    print ("   trg\t\t\t\t: print out measured phase of last trigger")
    print ("   clear\t\t\t\t\t: set last measured value to 0")
    print ("   debug\t\t\t: print out measured phase of last trigger + monitor32")
    print ("   loopback_test")
    print (" ")
    print ("Calibration")
    print ("   set ...")
    print ("       coarse <...> <0..9>\t: sets coarse delay adjustment for bin in the input registers")
    print ("       fine <...> <0..31>\t: sets fine delay adjustment for bin in the input registers")
    print (" ")
    print ("   read ...")
    print ("        calib\t\t: success of last self-calibration")
    print ("        fine <bin>\t: current value of fine delay alignment for bin (use dump_config for the input registers)")
    print ("        coarse <bin>\t: current value of coarse delay alignment for bin (use dump_config for the input registers)")
    print ("        config\t: print out all active config settings (loops on all currently activecoarse/fine adjust values)")
    print ("        last_bin\t\t\t: the latest bin for which delay alignments were accessed")
    print (" ")
    print ("Expert commands")
    print ("   set ...")
    print ("       calibrate <bit>: set self-calibration bit to 0/1")
    print ("       internal_clock_counter_offset <offset>\t: setting for clk_div value initialization")
    print ("       inv <...> <0/1>\t\t\t\t\t: inversion of fine delay signal for bin (should never be needed)")
    print ("       bin <...>\t: set active bin to set or read")
    print (" ")


# Calibration setters

if len(sys.argv) > 5 and sys.argv[1] == "phs2" and sys.argv[2] == "set" and sys.argv[3] == "coarse":
    uTTC.set_l1a_phase_v2_select(int(sys.argv[4]))
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(int(sys.argv[5]))

if len(sys.argv) > 5 and sys.argv[1] == "phs2" and sys.argv[2] == "set" and sys.argv[3] == "fine":
    uTTC.set_l1a_phase_v2_select(int(sys.argv[4]))
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(int(sys.argv[5]))

# Calibration getters

if len(sys.argv) > 3 and sys.argv[1] == "phs2" and sys.argv[2] == "read" and sys.argv[3] == "calib":
    print (uTTC.read_l1a_phase_v2_calibration_status())

if len(sys.argv) > 4 and sys.argv[1] == "phs2" and sys.argv[2] == "read" and sys.argv[3] == "fine":
    uTTC.set_l1a_phase_v2_select(int(sys.argv[4]))
    print (uTTC.read_l1a_phase_v2_fine_phase_adjust())

if len(sys.argv) > 4 and sys.argv[1] == "phs2" and sys.argv[2] == "read" and sys.argv[3] == "coarse":
    uTTC.set_l1a_phase_v2_select(int(sys.argv[4]))
    print (uTTC.read_l1a_phase_v2_coarse_phase_adjust())

if len(sys.argv) > 3 and sys.argv[1] == "phs2" and sys.argv[2] == "read" and sys.argv[3] == "config":
    print ("Trigger phase measurement module settings")
    uTTC.read_l1a_phase_v2_calibration_status()
    print ("Number of bits allocated for fine phase (starting from lsb, max 5):", uTTC.read_l1a_phase_v2_fine_phase_width())
    print ("Number of bins within a T_400MHz period:", uTTC.read_l1a_phase_v2_fine_phase_granularity())
    print ("Width of a bin (for self-calibration only):", uTTC.read_l1a_phase_v2_fine_phase_bin_size())
    print (" ")
    print ("Currently active Phase bin #/coarse adj./fine adj.")
    for i in range (0,uTTC.read_l1a_phase_v2_fine_phase_granularity()):
        uTTC.set_l1a_phase_v2_select(i)
        cta = uTTC.read_l1a_phase_v2_coarse_phase_adjust()
        fta = uTTC.read_l1a_phase_v2_fine_phase_adjust()
        print (i, "\t", cta, "\t", fta)

if len(sys.argv) > 3 and sys.argv[1] == "phs2" and sys.argv[2] == "read" and sys.argv[3] == "last_bin":
    print (uTTC.read_l1a_phase_v2_select())


# Commands

if len(sys.argv) > 2 and sys.argv[1] == "phs2" and sys.argv[2] == "load_settings":

#
# HERE WE SHOULD IMPLEMENT SOME TXT FILE READING FUNCTIONALITY
#
    uTTC.set_l1a_phase_v2_select(0)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(4)

    uTTC.set_l1a_phase_v2_select(1)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(7)

    uTTC.set_l1a_phase_v2_select(2)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(7)

    uTTC.set_l1a_phase_v2_select(3)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(8)

    uTTC.set_l1a_phase_v2_select(4)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(9)

    uTTC.set_l1a_phase_v2_select(5)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(12)

    uTTC.set_l1a_phase_v2_select(6)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(10)

    uTTC.set_l1a_phase_v2_select(7)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(13)

    uTTC.set_l1a_phase_v2_select(8)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(13)

    uTTC.set_l1a_phase_v2_select(9)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(15)

    uTTC.set_l1a_phase_v2_select(10)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(14)

    uTTC.set_l1a_phase_v2_select(11)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(17)

    uTTC.set_l1a_phase_v2_select(12)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(17)

    uTTC.set_l1a_phase_v2_select(13)
    uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(7)
    uTTC.set_l1a_phase_v2_fine_phase_adjust_input(18)

##     uTTC.set_l1a_phase_v2_select(14)
##     uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(2)
##     uTTC.set_l1a_phase_v2_fine_phase_adjust_input(29)

##     uTTC.set_l1a_phase_v2_select(15)
##     uTTC.set_l1a_phase_v2_coarse_phase_adjust_input(2)
##     uTTC.set_l1a_phase_v2_fine_phase_adjust_input(31)

    sys.exit()


if len(sys.argv) > 2 and sys.argv[1] == "phs2" and sys.argv[2] == "dump_settings":
    print ("Input register phase bin #/coarse adj./fine adj.")
    for i in range (0,uTTC.read_l1a_phase_v2_fine_phase_granularity()):
        uTTC.set_l1a_phase_v2_select(i)
        cta = uTTC.read_l1a_phase_v2_coarse_phase_adjust_input()
        fta = uTTC.read_l1a_phase_v2_fine_phase_adjust_input()
        print (i, "\t", cta, "\t", fta)


if len(sys.argv) > 2 and sys.argv[1] == "phs2" and sys.argv[2] == "configure":
    uTTC.set_l1a_phase_v2_self_calibrate(0)
    sys.exit()


if len(sys.argv) > 3 and sys.argv[1] == "phs2" and sys.argv[2] == "calibrate":

    uTTC.set_trigger_rule(0)
    uTTC.read_trigger_rule()
    uTTC.set_tts_fsm_disable()

    uTTC.set_trigger_source(7)
    uTTC.read_trigger_source()
    sel = sys.argv[3]
    if sel == "sfp+":
        uTTC.set_external_trigger_from_sfp()
    elif sel == "pin7":
        uTTC.set_external_trigger_from_fmc_l12_spare_pin7()
    else:
        uTTC.set_external_trigger_from_dio5()
    uTTC.read_external_trigger_select()
    uTTC.set_trigger_interval(1)
    uTTC.enable_triggers()
    uTTC.read_trigger_enable_state()

    uTTC.set_l1a_phase_v2_self_calibrate(1)
    sleep(2)
    print ("Calibration success: ", uTTC.read_l1a_phase_v2_calibration_status())

    uTTC.disable_triggers()

    for i in range (0,32):
    	uTTC.set_l1a_phase_v2_select(int(i))
    	uTTC.read_l1a_phase_v2_coarse_phase_adjust()
    	uTTC.read_l1a_phase_v2_fine_phase_adjust()
 
    uTTC.set_l1a_phase_v2_clear(1)
    uTTC.set_l1a_phase_v2_clear(0)



if len(sys.argv) > 2 and sys.argv[1] == "phs2" and sys.argv[2] == "trg":
    uTTC.l1a_phase_v2()


if len(sys.argv) > 2 and sys.argv[1] == "phs2" and sys.argv[2] == "clear":
    uTTC.clear_l1a_phase_v2()
    uTTC.l1a_phase_v2()


if len(sys.argv) > 2 and sys.argv[1] == "phs2" and sys.argv[2] == "debug":
    uTTC.l1a_phase_v2()
    uTTC.read_l1a_phase_v2_measurement_monitor32()


# Expert commands

if len(sys.argv) > 4 and sys.argv[1] == "phs2" and sys.argv[2] == "set" and sys.argv[3] == "calibrate":
   uTTC.set_l1a_phase_v2_self_calibrate(int(sys.argv[4]))


if len(sys.argv) > 3 and sys.argv[1] == "phs2" and sys.argv[2] == "internal_clock_counter_offset":
    uTTC.set_l1a_phase_v2_internal_clock_counter_offset(int(sys.argv[3]))


if len(sys.argv) > 5 and sys.argv[1] == "phs2" and sys.argv[2] == "set" and sys.argv[3] == "inv":
    uTTC.set_l1a_phase_v2_select(int(sys.argv[4]))
    uTTC.set_l1a_phase_v2_inv(int(sys.argv[5]))

if len(sys.argv) > 4 and sys.argv[1] == "phs2" and sys.argv[2] == "set" and sys.argv[3] == "bin":
    uTTC.set_l1a_phase_v2_select(int(sys.argv[4]))


if len(sys.argv) > 3 and sys.argv[1] == "phs2" and sys.argv[2] == "loopback_test":
    uTTC.disable_async_trigger()

    uTTC.set_trigger_source(6)
    uTTC.read_trigger_source()
    sel = sys.argv[3]
    if sel == "sfp+":
        uTTC.set_external_trigger_from_sfp()
    elif sel == "pin7":
        uTTC.set_external_trigger_from_fmc_l12_spare_pin7()
    else:
        uTTC.set_external_trigger_from_dio5()
    uTTC.read_external_trigger_select()
    uTTC.read_trigger_enable_state()
    uTTC.enable_triggers()
    uTTC.set_trigger_rule(0)
    uTTC.read_trigger_rule()
    uTTC.set_tts_fsm_disable()

    uTTC.set_async_trigger_num_triggers(1)
    uTTC.read_async_trigger_num_triggers()
    uTTC.set_async_trigger_spacing(10)
    uTTC.read_async_trigger_spacing()
    
    sleep(1)

    print ("coarse\tfine\tphase\t(coarse\t, fine)\tmon32")
    for c in range (0,10):
    	for f in range (0,uTTC.read_l1a_phase_v2_fine_phase_granularity()):
          uTTC.disable_async_trigger()
          uTTC.set_async_trigger_coarse_time_adjust(int(c))
          uTTC.set_async_trigger_fine_time_adjust(int(f))
          sleep(0.1)
          uTTC.enable_async_trigger()
          sleep(0.1)
          val_phase_avg = 0
          val_coarse_avg = 0
          val_fine_avg = 0
          val_mon32_and = 4294967295
          val_mon32_or = 0
          lsbs = 2**uTTC.read_l1a_phase_v2_fine_phase_width()
          nsteps = 20
          for i in range (0,nsteps):
              uTTC.disable_async_trigger()
              uTTC.enable_async_trigger()
              val_phase = uTTC.read_l1a_phase_v2_reg()
              val_phase_avg = val_phase_avg + val_phase
              val_coarse_avg = val_coarse_avg + val_phase // lsbs
              val_fine_avg = val_fine_avg + val_phase % lsbs
              val_mon32_and = val_mon32_and & uTTC.read_l1a_phase_v2_measurement_monitor32_reg()
              val_mon32_or = val_mon32_or | uTTC.read_l1a_phase_v2_measurement_monitor32_reg()

          val_phase_avg = val_phase_avg / nsteps
          val_coarse_avg = val_coarse_avg / nsteps
          val_fine_avg = val_fine_avg / nsteps
          val_mon32_or = val_mon32_and ^ val_mon32_or
          val_mon32_and = str('{:032b}'.format(val_mon32_and))
          val_mon32_or = str('{:032b}'.format(val_mon32_or))
          val_mon32_and = val_mon32_and[-uTTC.read_l1a_phase_v2_fine_phase_granularity():]
          val_mon32_or = val_mon32_or[-uTTC.read_l1a_phase_v2_fine_phase_granularity():]

          val_phase_std = 0
          val_coarse_std = 0
          val_fine_std = 0
          for i in range (0,nsteps):
              uTTC.disable_async_trigger()
              uTTC.enable_async_trigger()
              val_phase = uTTC.read_l1a_phase_v2_reg()
              val_phase_std = val_phase_std + (val_phase_avg - val_phase)**2
              val_coarse_std = val_coarse_std + (val_coarse_avg - val_phase // lsbs)**2
              val_fine_std = val_fine_std + (val_fine_avg - val_phase % lsbs)**2

          val_phase_std = val_phase_std / nsteps
          val_coarse_std = val_coarse_std / nsteps
          val_fine_std = val_fine_std / nsteps
          val_phase_std = math.ceil(math.sqrt(val_phase_std))
          val_coarse_std = math.ceil(math.sqrt(val_coarse_std))
          val_fine_std = math.ceil(math.sqrt(val_fine_std))

          print (c, "\t", f, "\t", val_phase_avg, "+/-",val_phase_std, "\t(", val_coarse_avg,"+/-",val_coarse_std, "\t", val_fine_avg,"+/-",val_fine_std, ")\t", val_mon32_and, "\t", val_mon32_or)
    uTTC.disable_async_trigger()


################################################################
### Orbit configuration
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "orb":
    print ("Commands")
    print ("    enable : enable bc0 (i.e. define orbit structure)")
    print ("    disable : disable bc0")
    print (" ")  
    print ("    stat : print out length <nbx> and gap <bcid>")
    print ("    set : ")
    print ("        length <nbx> : set length of an orbit in number of bx")
    print ("        gap_from <bcid> : set the start of the orbit gap at bcid")
    print ("        gap_to <bcid> : set the first valid bcid after the orbit gap")
    print (" ")
    print ("Expert commands")
    print (" ")
    print ("        bc0_command <16-bit cmd> : redefine beginning of orbit (e.g. bc0) command")

if len(sys.argv) > 2 and sys.argv[1] == "orb" and sys.argv[2] == "stat":
    uTTC.status_orb()

if len(sys.argv) > 2 and sys.argv[1] == "orb" and sys.argv[2] == "enable":
    uTTC.bc0_enable()

if len(sys.argv) > 2 and sys.argv[1] == "orb" and sys.argv[2] == "disable":
    uTTC.bc0_disable()

if len(sys.argv) > 4 and sys.argv[1] == "orb" and sys.argv[2] == "set" and sys.argv[3] == "length":
    uTTC.set_orbit_length(int(sys.argv[4]))

if len(sys.argv) > 4 and sys.argv[1] == "orb" and sys.argv[2] == "set" and sys.argv[3] == "gap_from":
    uTTC.set_abort_gap(int(sys.argv[4]))

if len(sys.argv) > 4 and sys.argv[1] == "orb" and sys.argv[2] == "set" and sys.argv[3] == "gap_to":
    uTTC.set_abort_gap_end(int(sys.argv[4]))

if len(sys.argv) > 4 and sys.argv[1] == "orb" and sys.argv[2] == "set" and sys.argv[3] == "bc0_command":
    uTTC.set_bc0_command(sys.argv[4])


################################################################
### BGo commands
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "bgo":
    print ("Commands")
    print ("    ec0 : ")
    print ("    bc0 : ")
    print ("    oc0 : ")
    print ("    resync : ")
    print ("    hard_reset : ")
    print ("    start_l1a : ")
    print ("    stop_l1a : ")
    print ("    calibration_trigger : ")
    print ("    reset : ")
    print ("    stop_seq : ")
    print ("    resync_seq : ")
    print (" ")
    print ("Expert commands")
    print (" ")

    
if len(sys.argv) > 2 and sys.argv[1] == "bgo" and sys.argv[2] == "ec0":
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x202)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bgo" and sys.argv[2] == "bc0":
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x204)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bgo" and sys.argv[2] == "oc0":
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x206)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bgo" and sys.argv[2] == "resync":
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0208)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bgo" and sys.argv[2] == "hard_reset":
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x2d0)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bgo" and sys.argv[2] == "start_l1a":
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x310)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bgo" and sys.argv[2] == "stop_l1a":
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x350)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bgo" and sys.argv[2] == "calibration_trigger":
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x390)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bgo" and sys.argv[2] == "reset":
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x20a)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bgo" and sys.argv[2] == "stop_seq":
    uTTC.send_ipb_BX(0x0000)
    uTTC.send_ipb_BCmd(0x0222)
    uTTC.send_ipb_BX(0x0000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bgo" and sys.argv[2] == "resync_seq":
    uTTC.send_ipb_BX(0x0000)
    uTTC.send_ipb_BCmd(0x0228)
    uTTC.send_ipb_BX(0x0000)
    uTTC.send_ipb_BCmd(0x0000)


################################################################
### B-channel primitive commands
################################################################

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "ec0bc0":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x00FD)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "periodic_ec0bc0":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x80fd)

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "ec0":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x009b)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "periodic_ec0":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x809b)

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "bc0":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x0067)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "periodic_bc0_on":
    uTTC.bc0_enable()

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "periodic_bc0_off":
    uTTC.bc0_disable()

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "oc0":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x0A1F)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "resync":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x1207)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "hard_reset":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x1A2B)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "start_l1a":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x220B)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "stop_l1a":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x2A27)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "calibration_trigger":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x323F)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "reset":
    uTTC.send_ipb_BX(0x7d0)
    uTTC.send_ipb_BCmd(0x1565)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)

if len(sys.argv) > 3 and sys.argv[1] == "bc0" and sys.argv[2] == "ChB_sendahead_time":	# 12 bits
    uTTC.send_ChB_proc_sendahead_time(int(sys.argv[3]))

if len(sys.argv) > 3 and sys.argv[1] == "bc0" and sys.argv[2] == "ChB_clk_sync_del":	#  2 bits
    uTTC.send_ChB_proc_clk_sync_del(int(sys.argv[3]))

if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "ChB_busy":
    uTTC.ChB_busy()


if len(sys.argv) > 2 and sys.argv[1] == "bc0" and sys.argv[2] == "stop_bcmd":
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)


################################################################
### Transition logger commands
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "log":
    print ("Commands")
    print ("    reset : reset FIFO block")
    print ("    size : print size of log FIFO")
    print ("    next : pop next element of log FIFO")
    print ("    dump : dump all elements of log FIFO (and clear FIFO)")
    print ("    fifo : raw dump of FIFO (and clear FIFO)")


if len(sys.argv) > 2 and sys.argv[1] == "log" and sys.argv[2] == "reset":
    uTTC.transistion_logger_reset_up()
    sleep(0.5)
    uTTC.transistion_logger_reset_down()


if len(sys.argv) > 2 and sys.argv[1] == "log" and sys.argv[2] == "size":
    uTTC.tr_logger_dcnt()


if len(sys.argv) > 2 and sys.argv[1] == "log" and sys.argv[2] == "next":
    uTTC.tr_logger_dout()


if len(sys.argv) > 2 and sys.argv[1] == "log" and sys.argv[2] == "dump":
    uTTC.tr_logger_dump()


if len(sys.argv) > 2 and sys.argv[1] == "log" and sys.argv[2] == "fifo":
    uTTC.tr_logger_dump_old()



################################################################
### TTS decoder and FSM commands
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "tts":
    print ("Commands")
    print ("    state\t\t : print value of last-received TTS state")
    print ("    enable_fsm\t : disable FSM reacting to TTS input from AMC13")
    print ("    disable_fsm\t : disable FSM reacting to TTS input from AMC13")
    print (" ")
    print ("Expert commands")
    print ("    set")
    print ("        idelay <value>\t : set input phase delay in case decoding is flaky (0..31)")
    print ("        idelay_inv 0/1\t : invert or not the idelay clock (default: 0)")
    print (" ")
    print ("    read")
    print ("        idelay\t : read input phase delay (0..31 in units of ~0.15 ns) currently being used")
    print ("        idelay_inv\t : read input phase delay inv setting")
    print (" ")


if len(sys.argv) > 2 and sys.argv[1] == "tts" and sys.argv[2] == "state":
    uTTC.read_tts_status_tts_state()


if len(sys.argv) > 2 and sys.argv[1] == "tts" and sys.argv[2] == "enable_fsm":
    uTTC.set_tts_fsm_enable()

if len(sys.argv) > 2 and sys.argv[1] == "tts" and sys.argv[2] == "disable_fsm":
    uTTC.set_tts_fsm_disable()


if len(sys.argv) > 4 and sys.argv[1] == "tts" and sys.argv[2] == "set" and sys.argv[3] == "idelay":
    uTTC.set_tts_idelay_intap(int(sys.argv[4]))
    uTTC.set_tts_idelay_load(1)
    sleep(0.2)
    uTTC.set_tts_idelay_load(0)


if len(sys.argv) > 4 and sys.argv[1] == "tts" and sys.argv[2] == "set" and sys.argv[3] == "idelay_inv":
    uTTC.set_tts_idelay_inv(int(sys.argv[4]))
    uTTC.set_tts_idelay_load(1)
    sleep(0.2)
    uTTC.set_tts_idelay_load(0)


if len(sys.argv) > 3 and sys.argv[1] == "tts" and sys.argv[2] == "read" and sys.argv[3] == "idelay":
    uTTC.read_tts_idelay_outtap()

if len(sys.argv) > 3 and sys.argv[1] == "tts" and sys.argv[2] == "read" and sys.argv[3] == "idelay_inv":
    uTTC.read_tts_idelay_inv()




################################################################################################################################
################################################################################################################################
################################################################################################################################
################################################################################################################################



#---------------------------------------------------------------------------------
###
### Debugging
###
#  Argument: testing TTS state machine
if len(sys.argv) > 1 and sys.argv[1] == "tts_test_1":
    # periodic BC0 ON
    uTTC.bc0_enable()
    # EC0
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x202)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)
    # OC0
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x206)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)
    # start user trigger
    #uTTC.set_trigger_source(7)
    #uTTC.set_trigger_interval(4000)
    #uTTC.enable_triggers()
    #uTTC.read_trigger_source()
    #uTTC.read_trigger_enable_state()
    #sleep(0.1)
    # stop user trigger
    #uTTC.set_trigger_interval(0)
    #uTTC.disable_triggers()
    #uTTC.read_trigger_source()
    #uTTC.read_trigger_enable_state()

    # sending triggers
    uTTC.set_trigger_source(1)
    uTTC.enable_triggers()
    # OC0
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x206)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)
    #
    uTTC.send_ipbus_trigger(0.1,500,0.01)
    uTTC.disable_triggers()

#  Argument: testing TTS state machine
if len(sys.argv) > 1 and sys.argv[1] == "tts_test_2":
    # periodic BC0 ON
    uTTC.bc0_enable()
    # EC0
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x202)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)
    # OC0
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x206)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)
    # start user trigger
    uTTC.set_trigger_source(7)
    uTTC.set_trigger_interval(4000)
    uTTC.enable_triggers()
    uTTC.read_trigger_source()
    uTTC.read_trigger_enable_state()
    sleep(3)
    # stop user trigger
    uTTC.set_trigger_interval(0)
    uTTC.disable_triggers()
    uTTC.read_trigger_source()
    uTTC.read_trigger_enable_state()

#  Argument: testing TTS state machine
if len(sys.argv) > 1 and sys.argv[1] == "tts_test_3":
    # periodic BC0 ON
    uTTC.bc0_enable()
    # EC0
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x202)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)
    # OC0
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x206)
    uTTC.send_ipb_BX(0x000)
    uTTC.send_ipb_BCmd(0x0000)
    # start user trigger
    uTTC.set_trigger_source(7)
    uTTC.set_trigger_interval(40000)
    uTTC.enable_triggers()
    uTTC.read_trigger_source()
    uTTC.read_trigger_enable_state()
    sleep(3)
    # stop user trigger
    #uTTC.set_trigger_interval(0)
    #uTTC.disable_triggers()
    #uTTC.read_trigger_source()
    #uTTC.read_trigger_enable_state()


#---------------------------------------------------------------------------------

#-- debug only

#  Argument: test L1A generator
if len(sys.argv) > 1 and sys.argv[1] == "debug_trigger_rule":

    uTTC.set_trigger_rule(0)
    uTTC.set_trigger_source(6)

    uTTC.set_async_trigger_coarse_time_adjust(1)
    uTTC.read_async_trigger_coarse_time_adjust()
    uTTC.set_async_trigger_spacing(2)
    uTTC.read_async_trigger_spacing()
    uTTC.set_async_trigger_num_triggers(1000)
    uTTC.read_async_trigger_num_triggers()

    uTTC.set_async_trigger_fine_time_adjust(0)
    uTTC.read_async_trigger_fine_time_adjust()

    uTTC.enable_triggers()

    uTTC.set_async_trigger_num_triggers(0)
    uTTC.read_async_trigger_num_triggers()
 
    uTTC.disable_triggers()


################################################################################################################################
################################################################################################################################
################################################################################################################################
################################################################################################################################

#print " "
#print "*** Error: does not compute"
#print " "
